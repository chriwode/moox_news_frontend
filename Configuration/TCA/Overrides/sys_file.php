<?php
/**
 * Add extra fields to the sys_category record
 */
$newSysFileColumns = array(
	'extension' => array(
		'label' => 'extension',
		'config' => array(
			'type' => 'passthrough'
		)
	),
);

// Add new TCA fields
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_file', $newSysFileColumns, 1);