<?php
defined('TYPO3_MODE') or die();

// set default language file as ll-reference
$ll = 'LLL:EXT:moox_news_frontend/Resources/Private/Language/locallang.xml:';

$tx_mooxnewsevent_domain_model_frontend = array(
    'fe_cruser' => array(
        'exclude' => 0,
        'label' => $ll . 'form.fe_user',
        'config' => array(
            'type' => 'select',
            'allowNonIdValues' => 1,
            'default' => '',
            'foreign_table' => 'fe_users',
            'foreign_table_where' => 'ORDER BY fe_users.username',
            'size' => 1,
            'maxitems' => 1,
            'minitems' => 0,
            'multiple' => 0,
            'items' => array(
                array('Keine Auswahl', ''),
            ),
        ),
        // special moox configuration
        'moox' => array(
            'extkey' => 'moox_news_frontend',
        ),
    ),
    'fe_crgroup' => array(
        'exclude' => 0,
        'label' => $ll . 'form.fe_crgroup',
        'config' => array(
            'type' => 'select',
            'allowNonIdValues' => 1,
            'default' => '',
            'foreign_table' => 'fe_groups',
            'foreign_table_where' => 'ORDER BY fe_groups.title',
            'size' => 1,
            'maxitems' => 1,
            'minitems' => 0,
            'multiple' => 0,
            'items' => array(
                array($ll . 'form.fe_crgroup.none', ''),
            ),
        ),
        // special moox configuration
        'moox' => array(
            'extkey' => 'moox_news_frontend',
            'variant' => 'moox_news',
            'plugins' => array(
                'mooxnewsfrontend' => array(
                    'add', 'edit', 'list', 'detail'
                ),
            ),
            'sortable' => 1,
        ),
    ),
    'paid' => array(
        'exclude' => 1,
        'l10n_mode' => 'mergeIfNotBlank',
        'label' => $ll . 'form.paid',
        'config' => array(
            'type' => 'input',
            'size' => 13,
            'max' => 20,
            'eval' => 'datetime',
            'checkbox' => 0,
            'default' => 0,
        ),
        // special moox configuration
        'moox' => array(
            'extkey' => 'moox_news_frontend',
        ),
    ),
    'paid_by' => array(
        'exclude' => 1,
        'label' => $ll . 'form.paid_by',
        'config' => array(
            'type' => 'select',
            'items' => array(
                array('Gruppen-Berechtigung', 0),
                array('Gutschein', 1),
                array('Payment', 2),
            ),
            'size' => 1,
            'maxitems' => 1,
            'default' => 0,
        ),
        // special moox configuration
        'moox' => array(
            'extkey' => 'moox_news_frontend',
        ),
    ),
    'paid_info' => array(
        'label' => 'paid info',
        'config' => array(
            'type' => 'passthrough'
        ),
    ),
    'privacy' => array(
        'exclude' => 1,
        'label' => $ll . 'form.privacy',
        'config' => array(
            'type' => 'select',
            'items' => array(
                array($ll . 'form.privacy.0', 0),
                array($ll . 'form.privacy.1', 1),
            ),
            'size' => 1,
            'maxitems' => 1,
            'default' => 0,
        ),
        // special moox configuration
        'moox' => array(
            'extkey' => 'moox_news_frontend',
            'variant' => 'moox_news',
            'plugins' => array(
                'mooxnewsfrontend' => array(
                    'add', 'edit', 'list', 'detail'
                ),
            ),
            'sortable' => 1,
        ),
    ),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'tx_mooxnews_domain_model_news',
    ',' . implode(',', array_keys($tx_mooxnewsevent_domain_model_frontend))
);

// extend tx_mooxnews_domain_model_news tca with new fields from this extension
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'tx_mooxnews_domain_model_news',
    $tx_mooxnewsevent_domain_model_frontend,
    true
);
