<?php
namespace DCNGmbH\MooxNewsFrontend\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Persistence\QueryInterface;
 
/**
 *
 *
 * @package moox_news_frontend
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class NewsRepository extends \Tx_MooxNews_Domain_Repository_NewsRepository {
	
	protected $defaultOrderings = array ('tstamp' => QueryInterface::ORDER_DESCENDING);
	
	/**
	 * sets query orderings from given array/string
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface &$query
	 * @param \array|\string|null	 
	 * @return \void
	 */
	protected function setQueryOrderings(\TYPO3\CMS\Extbase\Persistence\QueryInterface &$query, $orderings = NULL){
		
		$setOrderings = array();
		
		if(!is_null($orderings) && is_string($orderings)){
			
			$orderings = array($orderings => QueryInterface::ORDER_ASCENDING);
			
		}
		
		if(is_array($orderings)){
			
			foreach($orderings AS $field => $direction){				
				
				if(strtolower($direction)=="desc"){
						
					$setOrderings[$field] = QueryInterface::ORDER_DESCENDING;				
					
				} else {
						
					$setOrderings[$field] = QueryInterface::ORDER_ASCENDING;	
				}								
			}					
			
			if(count($setOrderings)){
				
				$query->setOrderings($setOrderings);
				
			}
		}		
	}	
	
	/**
	 * sets query limits from given values
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface &$query
	 * @param \integer $offset
	 * @param \integer $limit
	 * @return \void
	 */
	protected function setQueryLimits(\TYPO3\CMS\Extbase\Persistence\QueryInterface &$query, $offset = NULL, $limit = NULL){
	
		if(is_numeric($offset)){
			
			$query->setOffset($offset);
			
		}
		
		if(is_numeric($limit)){
			
			$query->setLimit($limit);
			
		}
	}
	
	/**
	 * sets query storage page(s)
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface &$query
	 * @param \array|\integer|\string $storagePages
	 * @return \void
	 */
	protected function setQueryStoragePages(\TYPO3\CMS\Extbase\Persistence\QueryInterface &$query, $storagePages = NULL){
		
		if(is_string($storagePages)){
			if($storagePages=="all"){
				$query->getQuerySettings()->setRespectStoragePage(FALSE);
			} elseif(strpos($storagePages, ",")!==false){
				$query->getQuerySettings()->setStoragePageIds(explode(",",$storagePages));
			}
		} elseif(is_array($storagePages)){
			
			$setStoragePages = array();
			
			foreach($storagePages AS $storagePage){
				
				if(is_numeric($storagePage)){
					
					$setStoragePages[] = $storagePage;
					
				}
			}
			
			if(count($setStoragePages)){
				
				$query->getQuerySettings()->setStoragePageIds($setStoragePages);
				
			}			
		
		} elseif(is_numeric($storagePages)){
			
			$query->getQuerySettings()->setStoragePageIds(array($storagePages));
			
		}				
	}
	
	/**
	 * Finds all by filter (ordered)
	 *	
	 * @param \array $filter
	 * @param \array $orderings
	 * @param \integer $offset
	 * @param \integer $limit
	 * @param \array|\integer $storagePages
	 * @param \array|\boolean $enableFieldsToBeIgnored
	 * @param \boolean $rawMode if set to true, return is as an array
	 * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByFilter($filter = NULL, $orderings = NULL, $offset = NULL, $limit = NULL, $storagePages = NULL, $enableFieldsToBeIgnored = NULL, $rawMode = FALSE) {
		
		$query = $this->createQuery();
		$this->setQueryStoragePages($query,$storagePages);	
		$this->setQueryOrderings($query,$orderings);		
		$this->setQueryLimits($query,$offset,$limit);
		
		if(is_array($enableFieldsToBeIgnored)){			
			$query->getQuerySettings()->setIgnoreEnableFields(TRUE);
			$query->getQuerySettings()->setEnableFieldsToBeIgnored($enableFieldsToBeIgnored);
		} elseif(!is_null($enableFieldsToBeIgnored) && $enableFieldsToBeIgnored){			
			$query->getQuerySettings()->setIgnoreEnableFields(TRUE);
			$query->getQuerySettings()->setEnableFieldsToBeIgnored(array("disabled","starttime","endtime"));
		}
		
		if($rawMode){
			$query->getQuerySettings()->setReturnRawQueryResult(TRUE);
		}
		
		$constraints = $this->createFilterConstraints($query,$filter);
		
		if(is_array($constraints)){
			
			return $query->matching(
				$query->logicalAnd($constraints)
			)->execute();
			
		} else {
			
			return $query->execute();	
			
		}
	}
	
	/**
	 * Returns a constraint array created by a given filter array
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $query
	 * @param \array $filter
	 * @param \array $constraints	
	 * @return \TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface|null
	 */
	protected function createFilterConstraints(\TYPO3\CMS\Extbase\Persistence\QueryInterface $query,$filter = NULL,$constraints = NULL){
				
		if(is_null($constraints)){
			
			$constraints = array();
			
		}
		
		if(isset($filter['feUser']) && is_object($filter['feUser'])){
			
			$constraints[] = $query->equals('feCruser', $filter['feUser']);
			
		}
		
		if(is_array($filter['feCrusers']) && count($filter['feCrusers']) && is_array($filter['feCrgroups']) && count($filter['feCrgroups'])){
			
			$tmpConstraints = array();
			$tmpConstraints[] = $query->logicalAnd(
				$query->greaterThan('feCrgroup', 0),
				$query->in('feCrgroup', $filter['feCrgroups'])
			);
			$tmpConstraints[] = $query->logicalAnd(
				$query->greaterThan('feCruser', 0),
				$query->in('feCruser', $filter['feCrusers']),
				$query->equals('feCrgroup', 0)
			);			
			if(isset($filter['own']) && is_object($filter['own'])){
				$tmpConstraints[] = $query->equals('feCruser', $filter['own']);
				$constraints[] = $query->logicalOr($tmpConstraints);
			} elseif(isset($filter['foreign']) && is_object($filter['foreign'])){
				$constraints[] = $query->logicalAnd(
					$query->logicalNot($query->equals('feCruser', $filter['foreign'])),
					$query->logicalOr($tmpConstraints)
				);
			} else {
				$constraints[] = $query->logicalOr($tmpConstraints);
			}
			
			
			
		} elseif(is_array($filter['feCrusers']) && count($filter['feCrusers'])){
			
			$tmpConstraints = array();
			if(isset($filter['own']) && is_object($filter['own'])){
				$tmpConstraints[] = $query->equals('feCruser', $filter['own']);				
			} elseif(isset($filter['foreign']) && is_object($filter['foreign'])){
				$tmpConstraints[] = $query->logicalNot($query->equals('feCruser', $filter['foreign']));				
			}
			$tmpConstraints[] = $query->greaterThan('feCruser', 0);
			$tmpConstraints[] = $query->in('feCruser', $filter['feCrusers']);
			$tmpConstraints[] = $query->equals('feCrgroup', 0);
			$constraints[] = $query->logicalAnd($tmpConstraints);
			
		} elseif(is_array($filter['feCrgroups']) && count($filter['feCrgroups'])){
			
			$tmpConstraints = array();
			if(isset($filter['own']) && is_object($filter['own'])){
				$tmpConstraints[] = $query->equals('feCruser', $filter['own']);				
			} elseif(isset($filter['foreign']) && is_object($filter['foreign'])){
				$tmpConstraints[] = $query->logicalNot($query->equals('feCruser', $filter['foreign']));				
			}
			$tmpConstraints[] = $query->greaterThan('feCrgroup', 0);
			$tmpConstraints[] = $query->in('feCrgroup', $filter['feCrgroups']);
			$constraints[] = $query->logicalAnd($tmpConstraints);			
		}
		
		if(isset($filter['feCruser']) && is_object($filter['feCruser'])){
			
			$constraints[] = $query->equals('feCruser', $filter['feCruser']);
			
		}
		
		if(isset($filter['feCrgroup']) && is_object($filter['feCrgroup'])){
			
			$constraints[] = $query->equals('feCrgroup', $filter['feCrgroup']);
			
		}

		if(isset($filter['categories']) && is_array($filter['categories']) && count($filter['categories'])){
			$categoryConstraints = array();
			foreach($filter['categories'] AS $category){
				$categoryConstraints[] = $query->contains('categories', $category);
			}
			$constraints[] = $query->logicalOr($categoryConstraints);
			
		}	
		
		if(isset($filter['type']) && is_string($filter['type']) && $filter['type']!=""){
			
			$constraints[] = $query->equals('type', $filter['type']);
			
		}
		
		if($filter['privacy']=="0"){
			
			$constraints[] = $query->equals('privacy', 0);
			
		} elseif($filter['privacy']=="1"){
			
			$constraints[] = $query->equals('privacy', 1);
			
		}
				
		if(isset($filter['types']) && is_array($filter['types']) && count($filter['types'])){
			
			$constraints[] = $query->in('type', $filter['types']);
			
		} elseif(isset($filter['types']) && is_string($filter['types'])){
			
			$constraints[] = $query->in('type', explode(",",$filter['types']));
			
		}
		
		if(count($constraints)<1){
			
			$constraints = NULL;
			
		}
		
		return $constraints;
	}	
	
	/**
	 * Override default findByUid function to enable also the option to turn of
	 * the enableField setting
	 *
	 * @param \integer $uid id of record
	 * @param \boolean $respectEnableFields if set to false, hidden records are shown
	 * @return \DCNGmbH\MooxNewsFrontend\Domain\Model\News
	 */
	public function findByUid($uid, $respectEnableFields = TRUE) {
		
		$query = $this->createQuery();
		
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(!$respectEnableFields);

		return $query->matching(
			$query->logicalAnd(
				$query->equals('uid', $uid),
				$query->equals('deleted', 0)
			))->execute()->getFirst();
	}	

	/**
	 * find extended filter items
	 *	
	 * @param \array $field
	 * @param \array $storagePages
	 * @param \string $table
	 * @param\ string $where	 
	 * @return array
	 */
	public function findTcaFieldValues($field,$storagePagess = NULL,$table,$where = "") {
		
		$result = array();
		
		if($table!=""){
			if(!is_array($storagePages)){
				$storagePages = array($storagePages);
			}
			
			$query = $this->createQuery();
					
			if(is_array($storagePages) && count($storagePage)){
				$query->getQuerySettings()->setRespectStoragePage(FALSE);
				$storagePageIdsStmt = " AND pid IN (".implode(",",$storagePages).")";
			}
			$query->getQuerySettings()->setRespectSysLanguage(TRUE);
			$query->getQuerySettings()->setIgnoreEnableFields(FALSE);
			$query->getQuerySettings()->setReturnRawQueryResult(TRUE);
			$query->statement('SELECT uid,title FROM `'.$table.'` WHERE 1=1'.$storagePageIdsStmt.' '.$where);
			$resultDb = $query->execute();
						
			foreach($resultDb AS $item){
				$result[] = array("uid" => $item['uid'], "title" => $item['title']);
			}
		}
		
		return $result;			
	}
}
?>