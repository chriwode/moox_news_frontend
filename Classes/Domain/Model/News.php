<?php
namespace DCNGmbH\MooxNewsFrontend\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *      2017 Christian Wolfram <c.wolfram@chriwo.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package moox_news_frontend
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class News extends \Tx_MooxNews_Domain_Model_News
{
    /**
     * fe user
     *
     * @var \DCNGmbH\MooxNewsFrontend\Domain\Model\FrontendUser
     */
    protected $feCruser = null;

    /**
     * fe crgroup
     *
     * @var \DCNGmbH\MooxNewsFrontend\Domain\Model\FrontendUserGroup
     */
    protected $feCrgroup = null;

    /**
     * paid
     *
     * @var int
     */
    protected $paid;

    /**
     * paid by
     *
     * @var int
     */
    protected $paidBy;

    /**
     * paid info
     *
     * @var string
     */
    protected $paidInfo;

    /**
     * privacy
     *
     * @var int
     */
    protected $privacy;

    /**
     * Returns the fe user
     *
     * @return \DCNGmbH\MooxNewsFrontend\Domain\Model\FrontendUser $feUser
     */
    public function getFeCruser()
    {
        return $this->feCruser;
    }

    /**
     * Sets the fe user
     *
     * @param \DCNGmbH\MooxNewsFrontend\Domain\Model\FrontendUser $feUser
     * @return void
     */
    public function setFeCruser(\DCNGmbH\MooxNewsFrontend\Domain\Model\FrontendUser $feUser)
    {
        $this->feCruser = $feUser;
    }

    /**
     * Returns the fe crgroup
     *
     * @return \DCNGmbH\MooxNewsFrontend\Domain\Model\FrontendUserGroup $feGroup
     */
    public function getFeCrgroup()
    {
        return $this->feCrgroup;
    }

    /**
     * Sets the fe crgroup
     *
     * @param null|\DCNGmbH\MooxNewsFrontend\Domain\Model\FrontendUserGroup $feGroup
     * @return void
     */
    public function setFeCrgroup($feGroup)
    {
        $this->feCrgroup = $feGroup;
    }

    /**
     * get paid
     *
     * @return int $paid paid
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * set paid
     *
     * @param int $paid paid
     * @return void
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;
    }

    /**
     * get paid by
     *
     * @return int $paid paid by
     */
    public function getPaidBy()
    {
        return $this->paidBy;
    }

    /**
     * set paid by
     *
     * @param int $paidBy paid by
     * @return void
     */
    public function setPaidBy($paidBy)
    {
        $this->paidBy = $paidBy;
    }

    /**
     * get paid info
     *
     * @return string $paidInfo paid info
     */
    public function getPaidInfo()
    {
        return $this->paidInfo;
    }

    /**
     * set paid info
     *
     * @param string $paidInfo paid info
     * @return void
     */
    public function setPaidInfo($paidInfo)
    {
        $this->paidInfo = $paidInfo;
    }

    /**
     * get privacy
     *
     * @return int $privacy privacy
     */
    public function getPrivacy()
    {
        return $this->privacy;
    }

    /**
     * set privacy
     *
     * @param int $privacy privacy
     * @return void
     */
    public function setPrivacy($privacy)
    {
        $this->privacy = $privacy;
    }
}
