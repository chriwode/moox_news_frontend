<?php
namespace DCNGmbH\MooxNewsFrontend\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *      2017 Christian Wolfram <c.wolfram@chriwo.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 *
 *
 * @package moox_news_frontend
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class FrontendUser extends \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser
{
    /**
     * crdate
     *
     * @var int
     */
    protected $crdate;

    /**
     * tstamp
     *
     * @var int
     */
    protected $tstamp;

    /**
     * usergroup
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxNewsFrontend\Domain\Model\FrontendUserGroup>
     */
    protected $usergroup = null;

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->usergroup = new ObjectStorage();
    }

    /**
     * get crdate
     *
     * @return int $crdate gender
     */
    public function getCrdate()
    {
        return $this->crdate;
    }

    /**
     * set crdate
     *
     * @param int $crdate crdate
     * @return void
     */
    public function setCrdate($crdate)
    {
        $this->crdate = $crdate;
    }

    /**
     * Get timestamp
     *
     * @return int
     */
    public function getTstamp()
    {
        return $this->tstamp;
    }

    /**
     * Set time stamp
     *
     * @param int $tstamp time stamp
     * @return void
     */
    public function setTstamp($tstamp)
    {
        $this->tstamp = $tstamp;
    }

    /**
     * set usergroup
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $usergroup
     * @return void
     */
    public function setUsergroup(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $usergroup)
    {
        $this->usergroup = $usergroup;
    }

    /**
     * add usergroup
     *
     * @param \DCNGmbH\MooxNewsFrontend\Domain\Model\FrontendUserGroup $usergroup
     * @return void
     */
    public function addUsergroup(\DCNGmbH\MooxNewsFrontend\Domain\Model\FrontendUserGroup $usergroup)
    {
        $this->usergroup->attach($usergroup);
    }

    /**
     * remove usergroup
     *
     * @param \DCNGmbH\MooxNewsFrontend\Domain\Model\FrontendUserGroup $usergroup
     * @return void
     */
    public function removeUsergroup(\DCNGmbH\MooxNewsFrontend\Domain\Model\FrontendUserGroup $usergroup)
    {
        $this->usergroup->detach($usergroup);
    }

    /**
     * remove usergroup
     *
     * @return \DCNGmbH\MooxNewsFrontend\Domain\Model\FrontendUserGroup $usergroup
     */
    public function getUsergroup()
    {
        return $this->usergroup;
    }
}
