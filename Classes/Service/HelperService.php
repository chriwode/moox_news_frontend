<?php
namespace DCNGmbH\MooxNewsFrontend\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Dominic Martin <dm@dcn.de>, DCN GmbH
 *      2017 Christian Wolfram <c.wolfram@chriwo.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 *
 *
 * @package moox_news_frontend
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class HelperService implements SingletonInterface
{
    /**
     * objectManager
     *
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    protected $objectManager;

    /**
     * configurationManager
     *
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
     */
    protected $configurationManager;

    /**
     * accessControllService
     *
     * @var \DCNGmbH\MooxNewsFrontend\Service\AccessControlService
     */
    protected $accessControllService;

    /**
     * frontendUserRepository
     *
     * @var \DCNGmbH\MooxNewsFrontend\Domain\Repository\FrontendUserRepository
     */
    protected $frontendUserRepository;

    /**
     * frontendUserGroupRepository
     *
     * @var \DCNGmbH\MooxNewsFrontend\Domain\Repository\FrontendUserGroupRepository
     */
    protected $frontendUserGroupRepository;

    /**
     * categoryRepository
     *
     * @var \DCNGmbH\MooxNewsFrontend\Domain\Repository\CategoryRepository
     */
    protected $categoryRepository;

    /**
     * newsRepository
     *
     * @var \DCNGmbH\MooxNewsFrontend\Domain\Repository\NewsRepository
     */
    protected $newsRepository;

    /**
     * configuration
     *
     * @var array
     */
    protected $configuration;

    /**
     * extConf
     *
     * @var boolean
     */
    protected $extConf;

    /**
     * storagePids
     *
     * @var array
     */
    protected $storagePids;

    /**
     * Path to the locallang file
     *
     * @var string
     */
    const LLPATH = 'LLL:EXT:moox_news_frontend/Resources/Private/Language/locallang.xlf:';

    /**
     *
     * @return void
     */
    public function initialize()
    {
        $this->objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
        $this->configurationManager = $this->objectManager->get(
            'TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface'
        );
        $this->accessControllService = $this->objectManager->get(
            'DCNGmbH\MooxNewsFrontend\Service\AccessControlService'
        );
        $this->frontendUserRepository = $this->objectManager->get(
            'DCNGmbH\MooxNewsFrontend\Domain\Repository\FrontendUserRepository'
        );
        $this->frontendUserGroupRepository = $this->objectManager->get(
            'DCNGmbH\MooxNewsFrontend\Domain\Repository\FrontendUserGroupRepository'
        );
        $this->categoryRepository = $this->objectManager->get(
            'DCNGmbH\MooxNewsFrontend\Domain\Repository\CategoryRepository'
        );
        $this->newsRepository = $this->objectManager->get('DCNGmbH\MooxNewsFrontend\Domain\Repository\NewsRepository');
        $this->configuration = $this->configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK,
            'MooxNewsFrontend'
        );

        // get extensions's configuration
        $this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_news_frontend']);
    }

    /**
     * prepare selected payment method
     *
     * @param array $payment payment
     * @param array &$messages messages
     * @param array &$errors errors
     * @return bool|array
     */
    public function preparePayment(array $payment, &$messages, &$errors)
    {
        $paymentProvider = $this->objectManager->get(
            'DCNGmbH\\' . GeneralUtility::underscoredToUpperCamelCase($payment['method']) . '\Service\PaymentProvider'
        );

        if (is_object($paymentProvider) && method_exists($paymentProvider, 'preparePayment')) {
            return $paymentProvider->preparePayment($payment, $messages, $errors);
        }

        return false;
    }

    /**
     * process selected payment method
     *
     * @param array $payment payment
     * @param array &$messages messages
     * @param array &$errors errors
     * @return bool|array
     */
    public function processPayment(array $payment, &$messages, &$errors)
    {
        $paymentProvider = $this->objectManager->get(
            'DCNGmbH\\' . GeneralUtility::underscoredToUpperCamelCase($payment['method']) . '\Service\PaymentProvider'
        );

        if (is_object($paymentProvider) && method_exists($paymentProvider, 'processPayment')) {
            return $paymentProvider->processPayment($payment, $messages, $errors);
        }
        return false;
    }

    /**
     * get payment configuration
     *
     * @param string $method method
     * @return array $config
     */
    public function getPaymentConfig($method)
    {
        $config = array();
        $paymentProvider = $this->objectManager->get(
            'DCNGmbH\\' . GeneralUtility::underscoredToUpperCamelCase($method) . '\Service\PaymentProvider'
        );

        if (is_object($paymentProvider)) {
            $config = $paymentProvider->getPaymentConfig();
        }

        return $config;
    }

    /**
     * check selected payment method
     *
     * @param array $payment payment
     * @param array &$messages messages
     * @param array &$errors errors
     * @return bool|array
     */
    public function checkPayment(array $payment, &$messages, &$errors)
    {
        $paymentProvider = $this->objectManager->get(
            'DCNGmbH\\' . GeneralUtility::underscoredToUpperCamelCase($payment['method']) . '\Service\PaymentProvider'
        );

        if (is_object($paymentProvider)) {
            if (method_exists($paymentProvider, 'checkPayment')) {
                return $paymentProvider->checkPayment($payment, $messages, $errors);
            }
        }
        return false;
    }

    /**
     * check dynamic form fields
     *
     * @param array $fields fields
     * @param array $item item
     * @param array &$messages messages
     * @param array &$errors errors
     * @return void
     */
    public function checkFields($fields = array(), $item = array(), &$messages, &$errors)
    {

        // initialize
        $this->initialize();

        // check fields
        foreach ($fields as $field) {
            if (in_array($field['config']['type'], array('check'))) {
                if (is_array($item[$field['key']])) {
                    $item[$field['key']] = implode(',', $item[$field['key']]);
                }
            }

            if (in_array($field['config']['type'], array('tree'))) {
                if (isset($field['config']['minitems']) && $field['config']['minitems'] > 0) {
                    $field['config']['required'] = 1;
                }
            }

            if (in_array($field['config']['type'], array('tree', 'file'))) {
                if ($field['config']['required'] && (!isset($field['config']['minitems']) || $field['config']['minitems'] < 1)) {
                    $field['config']['minitems'] = 1;
                }
            }

            if (in_array($field['config']['type'], array('file'))) {
                if ($field['config']['required'] && (!isset($field['config']['maxfilesize']) || $field['config']['maxfilesize'] < 1)) {
                    $field['config']['maxfilesize'] = 102400;
                }
            }

            $msgtitle = LocalizationUtility::translate(self::LLPATH . 'form.' . $field['key'], $this->extensionName);
            // set fallback title
            if (!$msgtitle) {
                $msgtitle = LocalizationUtility::translate(str_replace('moox_news_frontend', $field['extkey'],
                                                                       self::LLPATH
                                                           ) . 'form.' . $field['key'], $field['extkey']
                );
            }

            // check required fields only
            if ($field['config']['validator'] == 'tree' && ((isset($field['config']['minitems']) && $field['config']['minitems'] > 0) || (isset($field['config']['maxitems']) && $field['config']['maxitems'] > 0))) {
                if ($item[$field['key']] == '') {
                    $itemCnt = 0;
                } else {
                    $items = explode(',', $item[$field['key']]);
                    $itemCnt = count($items);
                }

                // if no file choosen
                if ($itemCnt < $field['config']['minitems']) {
                    $message = LocalizationUtility::translate(self::LLPATH . 'form.' . $field['key'] . '.error.too_few',
                                                              $this->extensionName, array($field['config']['minitems'])
                    );

                    // set fallback message
                    if (!$message) {
                        $message = LocalizationUtility::translate(str_replace('moox_news_frontend', $field['extkey'],
                                                                              self::LLPATH
                                                                  ) . 'form.' . $field['key'] . '.error.too_few',
                                                                  $field['extkey'], array($field['config']['minitems'])
                        );
                    }
                    if (!$message) {
                        $message = LocalizationUtility::translate(self::LLPATH . 'form.error.too_few',
                                                                  $this->extensionName,
                                                                  array($field['config']['minitems'])
                        );
                    }

                    // add message
                    $messages[] = array(
                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                        'title' => $msgtitle,
                        'text' => $message,
                        'type' => FlashMessage::ERROR
                    );

                    // set error
                    $errors[$field['key']] = true;
                }

                // if to many files choosen
                if ($item[$field['key']] != '' && isset($field['config']['maxitems']) && $itemCnt > $field['config']['maxitems']) {

                    // prepare message
                    $message = LocalizationUtility::translate(self::LLPATH . 'form.' . $field['key'] . '.error.too_many',
                                                              $this->extensionName, array($field['config']['maxitems'])
                    );

                    // set fallback message
                    if (!$message) {
                        $message = LocalizationUtility::translate(str_replace('moox_news_frontend', $field['extkey'],
                                                                              self::LLPATH
                                                                  ) . 'form.' . $field['key'] . '.error.too_many',
                                                                  $field['extkey'], array($field['config']['maxitems'])
                        );
                    }
                    if (!$message) {
                        $message = LocalizationUtility::translate(self::LLPATH . 'form.error.too_many',
                                                                  $this->extensionName,
                                                                  array($field['config']['maxitems'])
                        );
                    }

                    // add message
                    $messages[] = array(
                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                        'title' => $msgtitle,
                        'text' => $message,
                        'type' => FlashMessage::ERROR
                    );

                    // set error
                    $errors[$field['key']] = true;
                }

                // check required fields only
            } elseif ($field['config']['validator'] == 'file' && ((isset($field['config']['minitems']) && $field['config']['minitems'] > 0) || (isset($field['config']['maxitems']) && $field['config']['maxitems'] > 0) || (isset($field['config']['accepts']) && $field['config']['accepts'] != ''))) {
                $fileError = false;

                if ($item[$field['key']]['name'] != '' && $field['config']['accepts'] != '') {
                    $accepts = explode(',', strtolower($field['config']['accepts']));
                    $fileInfo = $this->getFileInfo($item[$field['key']]['name']);
                    if (!in_array($fileInfo['extension'], $accepts)) {

                        // prepare message
                        $message = LocalizationUtility::translate(self::LLPATH . 'form.' . $field['key'] . '.error.invalid',
                                                                  $this->extensionName,
                                                                  array($fileInfo['extension'], implode(',', $accepts))
                        );

                        // set fallback message
                        if (!$message) {
                            $message = LocalizationUtility::translate(str_replace('moox_news_frontend',
                                                                                  $field['extkey'], self::LLPATH
                                                                      ) . 'form.' . $field['key'] . '.error.invalid',
                                                                      $field['extkey'],
                                                                      array($fileInfo['extension'], implode(',',
                                                                                                            $accepts
                                                                      ))
                            );
                        }
                        if (!$message) {
                            $message = LocalizationUtility::translate(self::LLPATH . 'form.error.invalid',
                                                                      $this->extensionName,
                                                                      array($fileInfo['extension'], implode(',',
                                                                                                            $accepts
                                                                      ))
                            );
                        }

                        // add message
                        $messages[] = array(
                            'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                            'title' => $msgtitle,
                            'text' => $message,
                            'type' => FlashMessage::ERROR
                        );

                        // set error
                        $errors[$field['key']] = true;

                        $fileError = true;
                    }
                }

                if (!$fileError) {
                    if (!isset($item[$field['key'] . '_tmp']) || !is_array($item[$field['key'] . '_tmp'])) {
                        $fileCnt = 0;
                    } else {
                        $fileCnt = count($item[$field['key'] . '_tmp']);
                    }
                    if ($item[$field['key']]['name'] != '') {
                        $fileCnt++;
                    }

                    // if no file choosen
                    if ($fileCnt < $field['config']['minitems']) {

                        // prepare message
                        $message = LocalizationUtility::translate(self::LLPATH . 'form.' . $field['key'] . '.error.too_few',
                                                                  $this->extensionName,
                                                                  array($field['config']['minitems'])
                        );

                        // set fallback message
                        if (!$message) {
                            $message = LocalizationUtility::translate(str_replace('moox_news_frontend',
                                                                                  $field['extkey'], self::LLPATH
                                                                      ) . 'form.' . $field['key'] . '.error.too_few',
                                                                      $field['extkey'],
                                                                      array($field['config']['minitems'])
                            );
                        }
                        if (!$message) {
                            $message = LocalizationUtility::translate(self::LLPATH . 'form.error.too_few',
                                                                      $this->extensionName,
                                                                      array($field['config']['minitems'])
                            );
                        }

                        // add message
                        $messages[] = array(
                            'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                            'title' => $msgtitle,
                            'text' => $message,
                            'type' => FlashMessage::ERROR
                        );

                        // set error
                        $errors[$field['key']] = true;
                    }

                    // if to many files choosen
                    if ($item[$field['key']]['name'] != '' && isset($field['config']['maxitems']) && $fileCnt > $field['config']['maxitems']) {

                        // prepare message
                        $message = LocalizationUtility::translate(self::LLPATH . 'form.' . $field['key'] . '.error.too_many',
                                                                  $this->extensionName,
                                                                  array($field['config']['maxitems'])
                        );

                        // set fallback message
                        if (!$message) {
                            $message = LocalizationUtility::translate(str_replace('moox_news_frontend',
                                                                                  $field['extkey'], self::LLPATH
                                                                      ) . 'form.' . $field['key'] . '.error.too_many',
                                                                      $field['extkey'],
                                                                      array($field['config']['maxitems'])
                            );
                        }
                        if (!$message) {
                            $message = LocalizationUtility::translate(self::LLPATH . 'form.error.too_many',
                                                                      $this->extensionName,
                                                                      array($field['config']['maxitems'])
                            );
                        }

                        // add message
                        $messages[] = array(
                            'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                            'title' => $msgtitle,
                            'text' => $message,
                            'type' => FlashMessage::ERROR
                        );

                        // set error
                        $errors[$field['key']] = true;
                    }
                }

                // check required fields only
            } elseif (!in_array($field['config']['type'], array('file')
                ) && ($field['config']['required'] || isset($field['config']['maxlength']) || isset($field['config']['minlength']) || isset($field['config']['limit-low']) || isset($field['config']['limit-high']))
            ) {

                // check if field has a value
                if ($field['config']['required'] && (trim($item[$field['key']]
                        ) == '' || ($field['key'] == 'gender' && trim($item[$field['key']]) == 0))
                ) {

                    // prepare message
                    $message = LocalizationUtility::translate(self::LLPATH . 'form.' . $field['key'] . '.error.empty',
                                                              $this->extensionName
                    );

                    // set fallback message
                    if (!$message) {
                        $message = LocalizationUtility::translate(str_replace('moox_news_frontend', $field['extkey'],
                                                                              self::LLPATH
                                                                  ) . 'form.' . $field['key'] . '.error.empty',
                                                                  $field['extkey']
                        );
                    }
                    if (!$message) {
                        $message = LocalizationUtility::translate(self::LLPATH . 'form.error.empty',
                                                                  $this->extensionName
                        );
                    }

                    // add message
                    $messages[] = array(
                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                        'title' => $msgtitle,
                        'text' => $message,
                        'type' => FlashMessage::ERROR
                    );

                    // set error
                    $errors[$field['key']] = true;
                    // check if field value smaller than maxlength
                } elseif (trim($item[$field['key']]
                    ) != '' && isset($field['config']['maxlength']) && strlen(trim($item[$field['key']])
                    ) > $field['config']['maxlength']
                ) {

                    // prepare message
                    $message = LocalizationUtility::translate(self::LLPATH . 'form.' . $field['key'] . '.error.too_long',
                                                              $this->extensionName, array($field['config']['maxlength'])
                    );

                    // set fallback message
                    if (!$message) {
                        $message = LocalizationUtility::translate(str_replace('moox_news_frontend', $field['extkey'],
                                                                              self::LLPATH
                                                                  ) . 'form.' . $field['key'] . '.error.too_long',
                                                                  $field['extkey'], array($field['config']['maxlength'])
                        );
                    }
                    if (!$message) {
                        $message = LocalizationUtility::translate(self::LLPATH . 'form.error.too_long',
                                                                  $this->extensionName,
                                                                  array($field['config']['maxlength'])
                        );
                    }

                    // add message
                    $messages[] = array(
                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                        'title' => $msgtitle,
                        'text' => $message,
                        'type' => FlashMessage::ERROR
                    );

                    // set error
                    $errors[$field['key']] = true;

                    // check if field value larger than minlength
                } elseif (trim($item[$field['key']]
                    ) != '' && isset($field['config']['minlength']) && strlen(trim($item[$field['key']])
                    ) < $field['config']['minlength']
                ) {

                    // prepare message
                    $message = LocalizationUtility::translate(self::LLPATH . 'form.' . $field['key'] . '.error.too_short',
                                                              $this->extensionName, array($field['config']['minlength'])
                    );

                    // set fallback message
                    if (!$message) {
                        $message = LocalizationUtility::translate(str_replace('moox_news_frontend', $field['extkey'],
                                                                              self::LLPATH
                                                                  ) . 'form.' . $field['key'] . '.error.too_short',
                                                                  $field['extkey'], array($field['config']['minlength'])
                        );
                    }
                    if (!$message) {
                        $message = LocalizationUtility::translate(self::LLPATH . 'form.error.too_short',
                                                                  $this->extensionName,
                                                                  array($field['config']['minlength'])
                        );
                    }

                    // add message
                    $messages[] = array(
                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                        'title' => $msgtitle,
                        'text' => $message,
                        'type' => FlashMessage::ERROR
                    );

                    // set error
                    $errors[$field['key']] = true;

                    // check if field value greater than lowlimit
                } elseif (trim($item[$field['key']]
                    ) != '' && isset($field['config']['limit-low']) && trim($item[$field['key']]
                    ) < $field['config']['limit-low']
                ) {

                    // prepare message
                    $message = LocalizationUtility::translate(self::LLPATH . 'form.' . $field['key'] . '.error.too_small',
                                                              $this->extensionName, array($field['config']['limit-low'])
                    );

                    // set fallback message
                    if (!$message) {
                        $message = LocalizationUtility::translate(str_replace('moox_news_frontend', $field['extkey'],
                                                                              self::LLPATH
                                                                  ) . 'form.' . $field['key'] . '.error.too_small',
                                                                  $field['extkey'], array($field['config']['limit-low'])
                        );
                    }
                    if (!$message) {
                        $message = LocalizationUtility::translate(self::LLPATH . 'form.error.too_small',
                                                                  $this->extensionName,
                                                                  array($field['config']['limit-low'])
                        );
                    }

                    // add message
                    $messages[] = array(
                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                        'title' => $msgtitle,
                        'text' => $message,
                        'type' => FlashMessage::ERROR
                    );

                    // set error
                    $errors[$field['key']] = true;

                    // check if field value smaller than highlimit
                } elseif (trim($item[$field['key']]
                    ) != '' && isset($field['config']['limit-high']) && trim($item[$field['key']]
                    ) > $field['config']['limit-high']
                ) {

                    // prepare message
                    $message = LocalizationUtility::translate(self::LLPATH . 'form.' . $field['key'] . '.error.too_large',
                                                              $this->extensionName,
                                                              array($field['config']['limit-high'])
                    );

                    // set fallback message
                    if (!$message) {
                        $message = LocalizationUtility::translate(str_replace('moox_news_frontend', $field['extkey'],
                                                                              self::LLPATH
                                                                  ) . 'form.' . $field['key'] . '.error.too_large',
                                                                  $field['extkey'],
                                                                  array($field['config']['limit-high'])
                        );
                    }
                    if (!$message) {
                        $message = LocalizationUtility::translate(self::LLPATH . 'form.error.too_large',
                                                                  $this->extensionName,
                                                                  array($field['config']['limit-high'])
                        );
                    }

                    // add message
                    $messages[] = array(
                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                        'title' => $msgtitle,
                        'text' => $message,
                        'type' => FlashMessage::ERROR
                    );

                    // set error
                    $errors[$field['key']] = true;

                    // check if email is valid
                } elseif ($field['config']['validator'] == 'email' && !GeneralUtility::validEmail(trim($item[$field['key']]
                                                                                                  )
                    )
                ) {

                    // prepare message
                    $message = LocalizationUtility::translate(self::LLPATH . 'form.' . $field['key'] . '.error.invalid',
                                                              $this->extensionName
                    );

                    // set fallback message
                    if (!$message) {
                        $message = LocalizationUtility::translate(str_replace('moox_news_frontend', $field['extkey'],
                                                                              self::LLPATH
                                                                  ) . 'form.' . $field['key'] . '.error.invalid',
                                                                  $field['extkey']
                        );
                    }
                    if (!$message) {
                        $message = LocalizationUtility::translate(self::LLPATH . 'form.error.invalid',
                                                                  $this->extensionName
                        );
                    }

                    // add message
                    $messages[] = array(
                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                        'title' => $msgtitle,
                        'text' => $message,
                        'type' => FlashMessage::ERROR
                    );

                    // set error
                    $errors[$field['key']] = true;

                    // check if passwords are equal
                } elseif ($field['config']['validator'] == 'password' && trim($item[$field['key']]
                    ) != trim($item[$field['key'] . '_repeat'])
                ) {

                    // prepare message
                    $message = LocalizationUtility::translate(self::LLPATH . 'form.' . $field['key'] . '.error.not_equal',
                                                              $this->extensionName
                    );

                    // set fallback message
                    if (!$message) {
                        $message = LocalizationUtility::translate(str_replace('moox_news_frontend', $field['extkey'],
                                                                              self::LLPATH
                                                                  ) . 'form.' . $field['key'] . '.error.not_equal',
                                                                  $field['extkey']
                        );
                    }
                    if (!$message) {
                        $message = LocalizationUtility::translate(self::LLPATH . 'form.error.not_equal',
                                                                  $this->extensionName
                        );
                    }


                    // add message
                    $messages[] = array(
                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                        'title' => $msgtitle,
                        'text' => $message,
                        'type' => FlashMessage::ERROR
                    );

                    // set errors
                    $errors[$field['key']] = true;
                    $errors[$field['key'] . '_repeat'] = true;
                }
            }
        }
    }

    /**
     * set flash messages
     *
     * @param mixed &$flashMessageContainer
     * @param array $messages
     * @return void
     */
    public function setFlashMessages(&$flashMessageContainer = null, $messages = array())
    {
        if ($flashMessageContainer) {

            // set flash messages
            foreach ($messages as $message) {
                if (!is_array($message)) {
                    $message = array();
                }
                if ($message['text'] == '') {
                    $message['text'] = 'Unbekannter Fehler / Unknown error';
                }
                if ($message['icon'] != '' && $message['title'] != '') {
                    $message['title'] = $message['icon'] . $message['title'];
                }
                $flashMessageContainer->add($message['text'], ($message['title'] != '') ? $message['title'] . ': ' : '',
                                            $message['type'], true
                );
            }
        }
    }

    /**
     * get plugin field groups
     *
     * @param string $model
     * @param string $plugin
     * @param string $action
     * @return array $groups
     */
    public function getPluginFieldGroups($model = '', $plugin = '', $action = '')
    {
        $groups = array();

        if ($model != '' && $plugin != '' && is_array($GLOBALS['TCA'][$model]['moox']['fieldGroups']
            ) && count($GLOBALS['TCA'][$model]['moox']['fieldGroups'])
        ) {
            foreach ($GLOBALS['TCA'][$model]['moox']['fieldGroups'] as $groupname => $group) {
                if ($group['fields'] != '') {
                    $groups[] = array($group['label'], 'moox_fg_' . $groupname);
                }
            }
        }

        return $groups;
    }

    /**
     * get plugin fields
     *
     * @param string $model
     * @param string $plugin
     * @param string $action
     * @return array $fields
     */
    public function getPluginFields($model = '', $plugin = '', $action = '')
    {
        $fields = array();

        if ($model == '' && $plugin == '' && !is_array($GLOBALS['TCA'][$model]['columns'])
            && !count($GLOBALS['TCA'][$model]['columns'])
        ) {
            return $fields;
        }

        foreach ($GLOBALS['TCA'][$model]['columns'] as $fieldname => $field) {
            if (is_array($field['moox']['plugins'])) {
                if (($action == 'all' && (in_array($plugin, $field['moox']['plugins']) || is_array($field['moox']['plugins'][$plugin])))
                    || in_array($plugin, $field['moox']['plugins'])
                    || ($action != '' && is_array($field['moox']['plugins'][$plugin]) && in_array($action, $field['moox']['plugins'][$plugin]))
                ) {
                    $field['table'] = $model;
                    $fields[$fieldname] = $field;
                }
            }
        }

        return $fields;
    }

    /**
     * get field config
     *
     * @param array $fields
     * @param array $settings
     * @return array $fieldConfig
     */
    public function getFieldConfig($fields = array(), $settings = array())
    {
        $this->initialize();
        $fieldConfig = array();

        $hours = array();
        for ($i = 0; $i < 24; $i++) {
            $hours[str_pad($i, 2, '0', STR_PAD_LEFT)] = str_pad($i, 2, '0', STR_PAD_LEFT);
        }

        $minutes = array();
        for ($i = 0; $i < 60; $i += 5) {
            $minutes[str_pad($i, 2, '0', STR_PAD_LEFT)] = str_pad($i, 2, '0', STR_PAD_LEFT);
        }

        foreach ($fields as $fieldname => $field) {
            $config = array();

            if (!$field['moox']['header']) {
                $eval = array();
                if ($field['config']['eval'] != '') {
                    $eval = explode(',', $field['config']['eval']);
                }

                $config['variant'] = $field['moox']['variant'];
                $config['extkey'] = $field['moox']['extkey'];
                $config['extkeyUCC'] = GeneralUtility::underscoredToUpperCamelCase($config['extkey']);

                if (isset($field['displayCond'])) {
                    $config['conditions'] = array();
                    if (is_string($field['displayCond'])) {
                        if (substr($field['displayCond'], 0, 5) == 'FIELD') {
                            $displayCond = explode(':', $field['displayCond']);
                            if (in_array($displayCond[1], array('type'))) {
                                $config['conditions'][] = array('operator' => $displayCond[2], 'field' => $displayCond[1], 'equals' => $displayCond[3]);
                            }
                        }
                    } elseif (is_array($field['displayCond']) && is_array($field['displayCond']['OR'])) {
                        foreach ($field['displayCond']['OR'] as $or) {
                            if (substr($or, 0, 5) == 'FIELD') {
                                $displayCond = explode(':', $or);
                                if (in_array($displayCond[1], array('type'))) {
                                    $config['conditions'][] = array('operator' => $displayCond[2], 'field' => $displayCond[1], 'equals' => $displayCond[3]);
                                }
                            }
                        }
                    } elseif (is_array($field['displayCond']) && is_array($field['displayCond']['AND']
                        ) && is_array($field['displayCond']['AND']['OR'])
                    ) {
                        foreach ($field['displayCond']['AND']['OR'] as $or) {
                            if (substr($or, 0, 5) == 'FIELD') {
                                $displayCond = explode(':', $or);
                                if (in_array($displayCond[1], array('type'))) {
                                    $config['conditions'][] = array('operator' => $displayCond[2], 'field' => $displayCond[1], 'equals' => $displayCond[3]);
                                }
                            }
                        }
                    }
                    if (!count($config['conditions'])) {
                        unset($config['conditions']);
                    }
                }

                if ($fieldname == 'email') {
                    $config['type'] = 'email';
                    $config['validator'] = 'email';
                    $config['data']['data-type'] = 'email';
                    $config['data']['data-validator'] = 'email';
                } elseif (in_array('email', $eval)) {
                    $config['type'] = 'email';
                    $config['validator'] = 'email';
                    $config['data']['data-type'] = 'email';
                    $config['data']['data-validator'] = 'email';
                } elseif (in_array('date', $eval)) {
                    $config['type'] = 'date';
                    $config['hours'] = $hours;
                    $config['minutes'] = $minutes;
                    $config['data']['data-type'] = 'date';
                } elseif (in_array('datetime', $eval)) {
                    $config['type'] = 'datetime';
                    $config['hours'] = $hours;
                    $config['minutes'] = $minutes;
                    $config['data']['data-type'] = 'datetime';
                } elseif ($field['config']['type'] == 'inline' && $field['config']['foreign_table'] == 'sys_file_reference') {
                    $config['type'] = 'file';
                    $config['reference-type'] = ($field['config']['reference']) ? $field['config']['reference'] : 'file';
                    $config['validator'] = 'file';
                    $config['data']['data-type'] = 'file';
                    $config['data']['data-validator'] = 'file';
                } elseif ($fieldname == 'categories') {
                    $config['type'] = 'tree';
                    $config['validator'] = 'tree';
                    $config['data']['data-type'] = 'tree';
                    $config['data']['data-validator'] = 'tree';
                    $config['items'] = $this->makeTree($this->categoryRepository->findByFilter(null, 'parent', null,
                                                                                               null, $this->storagePids
                    )
                    );
                } elseif ($field['config']['type'] == 'check') {
                    $config['type'] = 'check';
                    $config['validator'] = 'check';
                    $config['data']['data-type'] = 'check';
                    $config['data']['data-validator'] = 'check';
                } elseif ($field['config']['type'] == 'text') {
                    if ((isset($field['defaultExtras']) && strpos($field['defaultExtras'], 'richtext'
                            ) !== false) || is_array($field['config']['wizards']['RTE'])
                    ) {
                        $config['type'] = 'editor';
                    } else {
                        $config['type'] = 'textarea';
                    }
                    $config['data']['data-type'] = 'text';
                } elseif ($field['config']['type'] == 'select') {
                    $config['type'] = 'select';
                    $config['data']['data-type'] = 'select';
                } elseif ($field['config']['type'] == 'radio') {
                    $config['type'] = 'radio';
                    $config['data']['data-type'] = 'radio';
                } else {
                    $config['type'] = 'text';
                    $config['data']['data-type'] = 'text';
                }

                if ($field['config']['unique']) {
                    $config['unique'] = 1;
                    $config['data']['data-unique'] = 1;
                }

                if (in_array($field['config']['type'], array('select', 'radio'))) {
                    if (is_array($field['config']['items'])) {
                        $config['items'] = array();
                        foreach ($field['config']['items'] as $item) {
                            if (substr($item[0], 0, 4) == 'LLL:') {
                                $label = LocalizationUtility::translate($item[0], $config['extkeyUCC']);
                            } else {
                                $label = $item[0];
                            }
                            $config['items'][$item[1]] = $label;
                        }
                        if ($field['config']['foreign_table'] != '') {
                            $itemsFromDb = $this->newsRepository->findTcaFieldValues($fieldname, $this->storagePids,
                                                                                     $field['config']['foreign_table'],
                                                                                     $field['config']['foreign_table_where']
                            );
                            foreach ($itemsFromDb as $item) {
                                $config['items'][$item['uid']] = $item['title'];
                            }
                        }
                    }
                }

                if ($field['config']['type'] == 'check') {
                    if (is_array($field['config']['items'])) {
                        $config['items'] = array();
                        foreach ($field['config']['items'] as $item) {
                            if (substr($item[0], 0, 4) == 'LLL:') {
                                $label = LocalizationUtility::translate($item[0], $config['extkeyUCC']);
                            } else {
                                $label = $item[0];
                            }
                            $config['items'][$item[1]] = $label;
                        }
                        if ($field['config']['foreign_table'] != '') {
                            $itemsFromDb = $this->newsRepository->findTcaFieldValues($fieldname, $this->storagePids,
                                                                                     $field['config']['foreign_table'],
                                                                                     $field['config']['foreign_table_where']
                            );
                            foreach ($itemsFromDb as $item) {
                                $config['items'][$item['uid']] = $item['title'];
                            }
                        }
                    } else {
                        $label_select = self::LLPATH . 'form.' . $fieldname . '.select';
                        $label_select_test = LocalizationUtility::translate($label_select, $config['extkeyUCC']);
                        if ($label_select_test == '') {
                            $label_select = self::LLPATH . 'form.' . $fieldname;
                        }
                        $config['label-select'] = $label_select;

                        $label_0 = LocalizationUtility::translate(self::LLPATH . 'form.' . $fieldname . '.select.0',
                                                                  $config['extkeyUCC']
                        );
                        if ($label_0 == '') {
                            $label_0 = LocalizationUtility::translate(self::LLPATH . 'form.deactivated',
                                                                      $config['extkeyUCC']
                            );
                        }
                        $label_1 = LocalizationUtility::translate(self::LLPATH . 'form.' . $fieldname . '.select.1',
                                                                  $config['extkeyUCC']
                        );
                        if ($label_1 == '') {
                            $label_1 = LocalizationUtility::translate(self::LLPATH . 'form.activated',
                                                                      $config['extkeyUCC']
                            );
                        }

                        if (!in_array($fieldname, array('istopnews'))) {
                            $config['items'] = array();
                            if (in_array($fieldname, array('hidden', 'istopnews'))) {
                                $config['items'][0] = $label_0;
                            }
                            $config['items'][1] = $label_1;
                        }
                    }
                }

                if (in_array('date', $eval) || in_array($fieldname, array('crdate', 'tstamp'))) {
                    $config['format'] = 'date';
                } elseif (in_array('datetime', $eval)) {
                    $config['format'] = 'datetime';
                } elseif (in_array($field['config']['type'], array('select', 'radio'))) {
                    $config['format'] = 'selected';
                } elseif (in_array($field['config']['type'], array('check'))) {
                    $config['format'] = 'checked';
                } elseif ($config['type'] == 'file') {
                    $config['format'] = 'file';
                } else {
                    $config['format'] = 'text';
                }

                if ($settings[$fieldname . 'Minlength'] > 0) {
                    $config['minlength'] = $settings[$fieldname . 'Minlength'];
                    $config['data']['data-minlength'] = $settings[$fieldname . 'Minlength'];
                } elseif ($field['config']['min'] > 0) {
                    $config['minlength'] = $field['config']['min'];
                    $config['data']['data-minlength'] = $field['config']['min'];
                }
                if ($field['config']['max'] > 0) {
                    $config['maxlength'] = $field['config']['max'];
                    $config['data']['data-maxlength'] = $field['config']['max'];
                }
                if ($field['config']['range']['lower'] > 0) {
                    $config['limit-low'] = $field['config']['range']['lower'];
                    $config['data']['data-limit-low'] = $field['config']['range']['lower'];
                }
                if ($field['config']['range']['upper'] > 0) {
                    $config['limit-high'] = $field['config']['range']['upper'];
                    $config['data']['data-limit-high'] = $field['config']['range']['upper'];
                }
                if ($field['config']['maxitems'] > 0) {
                    $config['maxitems'] = $field['config']['maxitems'];
                    $config['data']['data-maxitems'] = $field['config']['maxitems'];
                }
                if ($field['config']['minitems'] > 0) {
                    $config['minitems'] = $field['config']['minitems'];
                    $config['data']['data-minitems'] = $field['config']['minitems'];
                }
                if ($field['config']['maxfilesize'] > 0) {
                    $config['maxfilesize'] = $field['config']['maxfilesize'];
                    $config['data']['data-maxfilesize'] = $field['config']['maxfilesize'];
                }
                if ($field['config']['accepts'] != '') {
                    $config['accepts'] = $field['config']['accepts'];
                    $config['data']['accept'] = $field['config']['accepts'];
                }
                if ($field['config']['cols'] != '') {
                    $config['cols'] = $field['config']['cols'];
                }
                if ($field['config']['rows'] != '') {
                    $config['rows'] = $field['config']['rows'];
                }
                if ($field['moox']['default'] != '') {
                    $config['default'] = $field['moox']['default'];
                }
            } else {
                $config['type'] = $field['moox']['type'];
                $config['extkey'] = $field['moox']['extkey'];
                $config['extkeyUCC'] = GeneralUtility::underscoredToUpperCamelCase($config['extkey']);
                $config['type'] = 'header';
                $config['passthrough'] = 1;
            }

            $fieldConfig[$fieldname] = $config;
        }

        return $fieldConfig;
    }

    /**
     * configure fields
     *
     * @param string $fieldlist
     * @param string $requiredFieldlist
     * @param array $settings
     * @param array $fieldConfig
     * @param string $type
     * @param array $allowedTypes
     * @return array $configuredFields
     */
    public function configureFields(
        $fieldlist = '',
        $requiredFieldlist = '',
        $settings = array(),
        $fieldConfig = array(),
        $type = '',
        $allowedTypes = array()
    ) {
        $configuredFields = array();
        $requiredFields = array();

        // if fiels in field list
        if ($fieldlist != '') {
            $fields = explode(',', $fieldlist);
        } else {
            $fields = array();
        }

        // if fiels in required field list
        if ($requiredFieldlist != '') {
            $requiredFields = explode(',', $requiredFieldlist);
        }

        // prepare group fields
        $reinitFieldlist = false;
        foreach ($fields as $field) {
            if (substr($field, 0, 8) == 'moox_fg_') {
                $groupFields = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['fieldGroups'][substr($field, 8
                )]['fields'];
                if ($groupFields != '') {
                    $groupFieldsStr = '';
                    $groupFields = explode(',', $groupFields);
                    for ($i = 0; $i < count($groupFields); $i++) {
                        if ($i > 0) {
                            $groupFieldsStr .= ',';
                        }
                        if ($i == 0) {
                            $groupFieldsStr .= 'moox_fg_start.' . substr($field, 8) . '.';
                        }
                        if ($i == (count($groupFields) - 1)) {
                            $groupFieldsStr .= 'moox_fg_end.' . substr($field, 8) . '.';
                        }
                        $groupFieldsStr .= $groupFields[$i];
                    }
                    $fieldlist = str_replace($field, $groupFieldsStr, $fieldlist);
                    $reinitFieldlist = true;
                }
            }
        }
        if ($reinitFieldlist) {
            $fields = explode(',', $fieldlist);
        }

        foreach ($fields as $field) {
            $isGroupStart = false;
            $isGroupEnd = false;
            $checkForGroup = explode('.', $field);
            if (count($checkForGroup) > 1) {
                if ($checkForGroup[0] == 'moox_fg_start') {
                    $groupLabel = LocalizationUtility::translate($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['fieldGroups'][$checkForGroup[1]]['label'],
                                                                 $this->extensionName
                    );
                    $isGroupStart = true;
                    $field = $checkForGroup[2];
                    $group = $checkForGroup[1];
                } elseif ($checkForGroup[0] == 'moox_fg_end') {
                    $isGroupEnd = true;
                    $field = $checkForGroup[2];
                    $group = $checkForGroup[1];
                }
            }

            if (isset($fieldConfig[$field])) {
                $config = $fieldConfig[$field];
            } else {
                $config = array('type' => 'text', 'data' => array('data-type' => 'text'));
            }

            if (is_array($allowedTypes) && count($allowedTypes) && $field == 'type') {
                foreach ($config['items'] as $key => $value) {
                    if (!in_array(GeneralUtility::underscoredToUpperCamelCase($key), $allowedTypes)) {
                        unset($config['items'][$key]);
                    }
                }
            }

            // get only admin groups
            if ($field == 'fe_crgroup' && true === $this->accessControllService->hasLoggedInFrontendUser()) {
                $feUser = $this->frontendUserRepository->findByUid($this->accessControllService->getFrontendUserUid());

                if ($feUser) {
                    // set variant filter
                    $filter['variant'] = 'moox_community';

                    // set community admin or moderator filter
                    $filter['communityAdminOrModerator'] = $feUser;

                    // get admin groups
                    $feCrgroups = $this->frontendUserGroupRepository->findByFilter($filter, $this->orderings, null,
                                                                                   null, 'all', null
                    );

                    // get admin group uids
                    $feGroups = array();
                    foreach ($feCrgroups as $feGroup) {
                        $feGroups[] = $feGroup->getUid();
                    }

                    foreach ($config['items'] as $key => $value) {
                        if ($key != '' && !in_array($key, $feGroups)) {
                            unset($config['items'][$key]);
                        }
                    }
                }
            }

            if ($type == '' || $config['extkey'] == $type || $config['variant'] == $type || $config['variant'] == 'moox_news' || $config['extkey'] == 'moox_news') {
                if (count($config['conditions']) && $type != '') {
                    $show = false;
                    foreach ($config['conditions'] as $condition) {
                        if ($condition['field'] == 'type') {
                            if ($condition['operator'] == '=' && $type == $condition['equals']) {
                                $show = true;
                                break;
                            }
                            if ($condition['operator'] == '!=' && $type != $condition['equals']) {
                                $show = true;
                                break;
                            }
                        }
                    }
                } else {
                    $show = true;
                }

                if ($show) {
                    if (in_array($field, $requiredFields)) {
                        $config['required'] = 1;
                    } else {
                        $config['required'] = 0;
                    }
                    $config['data']['data-id'] = $field;
                    $config['data']['data-required'] = $config['required'];
                    if ($config['required'] || in_array($config['data']['data-validator'], array('email')
                        ) || $config['data']['data-minlength'] || $config['data']['data-maxlength'] || $config['data']['data-limit-low'] || $config['data']['data-limit-hight'] || $config['data']['data-minitems'] || $config['data']['data-maxitems']
                    ) {
                        $config['validate'] = 1;
                    } else {
                        $config['validate'] = 0;
                    }
                    if ($config['extkey'] != 'moox_news_frontend') {
                        $config['data']['data-label'] = LocalizationUtility::translate(str_replace('moox_news_frontend',
                                                                                                   $config['extkey'],
                                                                                                   self::LLPATH
                                                                                       ) . 'form.' . $field,
                                                                                       $config['extkeyUCC']
                        );
                    } else {
                        $config['data']['data-label'] = LocalizationUtility::translate(self::LLPATH . 'form.' . $field,
                                                                                       $config['extkeyUCC']
                        );
                    }
                    if ($config['type'] == 'header') {
                        $labelPrefix = 'header';
                    } else {
                        $labelPrefix = 'form';
                    }
                    if ($isGroupStart) {
                        $configuredFields[$field] = array('key' => (string)$field, 'extkey' => (string)$config['extkey'], 'type' => (string)$config['type'], 'label' => $labelPrefix . '.' . $field, 'group' => $group, 'is-group-start' => 1, 'label-group' => $groupLabel, 'config' => $config);
                    } elseif ($isGroupEnd) {
                        $configuredFields[$field] = array('key' => (string)$field, 'extkey' => (string)$config['extkey'], 'type' => (string)$config['type'], 'label' => $labelPrefix . '.' . $field, 'group' => $group, 'is-group-end' => 1, 'config' => $config);
                    } else {
                        $configuredFields[$field] = array('key' => (string)$field, 'extkey' => (string)$config['extkey'], 'type' => (string)$config['type'], 'label' => $labelPrefix . '.' . $field, 'config' => $config);
                    }
                }
            }
        }

        return $configuredFields;
    }

    /**
     * Get file info
     *
     * @param string $filename
     * @return array file
     */
    public function getFileInfo($filename)
    {
        $seperatorIndex = strrpos($filename, '.');
        $file['name'] = substr($filename, 0, $seperatorIndex);
        $file['extension'] = strtolower(substr($filename, ($seperatorIndex + 1)));
        return $file;
    }

    /**
     * Get available types
     *
     * @param array $exclude
     * @return array $types
     */
    public function getAvailableTypes($exclude = null)
    {
        $types = array();

        if (is_string($exclude) && $exclude != '') {
            $exclude = explode(',', $exclude);
        } elseif (is_string($exclude) && $exclude == '') {
            $exclude = array();
        }

        foreach ($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns']['type']['config']['items'] as $type) {
            $types[] = array($GLOBALS['LANG']->sL($type[0], true), $type[1]);
        }

        return $types;
    }

    /**
     * Get timestamp by date and time strings
     *
     * @param string $date
     * @param string $hours
     * @param string $minutes
     * @param string $format
     * @return string $timestamp
     */
    public function getTimestamp($date = '', $hours = '', $minutes = '', $format = 'd.m.Y H:i')
    {
        $timestamp = '';

        if ($date != '') {
            if ($hours != '' && $minutes != '') {
                $string = $date . ' ' . $hours . ':' . $minutes;
            } else {
                $string = $date;
            }
            $datetime = date_parse_from_format($format, $string);
            $timestamp = mktime($datetime['hour'], $datetime['minute'], 0, $datetime['month'], $datetime['day'],
                                $datetime['year']
            );
        }

        return $timestamp;
    }

    /**
     * copy to temp
     *
     * @param string $source
     * @param string $filename
     * @param string $folder
     * @return array file
     */
    public function copyToTemp($source, $filename, $folder)
    {
        $filenameParts = explode('.', $filename);

        $tempfile = array();
        $tempfile['folder'] = '/typo3temp/' . $folder;
        $tempfile['absolute'] = $GLOBALS['_SERVER']['DOCUMENT_ROOT'] . $tempfile['folder'];
        $tempfile['name'] = md5($filenameParts[0] . filesize($source) . date('Ymdhis')) . '.' . $filenameParts[1];
        $tempfile['path'] = $tempfile['absolute'] . '/' . $tempfile['name'];
        $tempfile['src'] = $tempfile['folder'] . '/' . $tempfile['name'];

        if (!is_dir($tempfile['absolute'])) {
            mkdir($tempfile['absolute']);
        }

        if (file_exists($tempfile['path'])) {
            unlink($tempfile['path']);
        }

        if (!file_exists($tempfile['path'])) {
            move_uploaded_file($source, $tempfile['path']);
        }

        return $tempfile;
    }

    /**
     * make tree
     *
     * @param array|object $items
     * @return array tree
     */
    public function makeTree($items = null)
    {
        $tree = array();

        if (is_object($items)) {
            if ($items->count() > 0) {
                foreach ($items as $item) {
                    $tree[(int)$item->getUid()]['item'] = $item;
                    if ($item->getParentcategory()) {
                        $tree[(int)$item->getParentcategory()->getUid()]['children'][(int)$item->getUid(
                        )] =& $tree[(int)$item->getUid()];
                    } else {
                        $tree[0]['children'][(int)$item->getUid()] =& $tree[(int)$item->getUid()];
                    }
                }

                $tree = $tree[0]['children'];
            }
        } else {
            if (count($items)) {
                foreach ($items as $item) {
                    $tree[(int)$item['uid']]['item'] = $item;
                    $tree[(int)$item['parent']]['children'][(int)$item['uid']] =& $tree[(int)$item['uid']];
                }

                $tree = $tree[0]['children'];
            }
        }

        return $tree;
    }

    /**
     * Get array of folders with special module
     *
     * @param string $module
     * @return array folders
     */
    public function getFolders($module = '')
    {
        global $BE_USER;

        $folders = array();

        if ($module != '') {
            $query = array(
                'SELECT' => '*',
                'FROM' => 'pages',
                'WHERE' => $BE_USER->getPagePermsClause(1
                    ) . ' AND deleted=0 AND doktype=254 AND module="' . $module . '"',
            );
            $pages = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($query);

            while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($pages)) {
                $folders[] = $row;
            }
        }

        return $folders;
    }

    /**
     * hide tca fields off parent type
     *
     * @param string $hideFields
     * @param string $table
     * @param string $type
     * @param string $operator
     * @return void
     */
    public function tcaHideDefaultFields($hideFields = '', $table = '', $type = '', $operator = '=')
    {
        // hide classified default fields defined in hide fields string
        if ($hideFields != '') {
            $hideFields = explode(',', $hideFields);
            foreach ($hideFields as $hideField) {
                if (isset($GLOBALS['TCA'][$table]['columns'][$hideField])) {
                    if (!isset($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'])) {
                        $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'] = '';
                    }
                    if (is_string($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']
                        ) && $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'] == ''
                    ) {
                        $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'] = 'FIELD:type:' . $operator . ':' . $type;
                    } elseif (is_string($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']
                        ) && $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'] != ''
                    ) {
                        $displayCond = $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'];
                        $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'] = array();
                        $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'] = array();
                        $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'][] = $displayCond;
                        $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'][] = 'FIELD:type:' . $operator . ':' . $type;
                        $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'] = array_unique($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR']
                        );
                    } elseif (is_array($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']
                        ) && is_array($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'])
                    ) {
                        $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'][] = $displayCond;
                        $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'][] = 'FIELD:type:' . $operator . ':' . $type;
                        $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'] = array_unique($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR']
                        );
                    } elseif (is_array($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']
                        ) && is_array($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['AND'])
                    ) {
                        if (!is_array($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['AND']['OR'])) {
                            $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['AND']['OR'] = array();
                        }
                        $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['AND']['OR'][] = 'FIELD:type:' . $operator . ':' . $type;
                        $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['AND']['OR'] = array_unique($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['AND']['OR']
                        );
                    }
                }
            }
        }
    }

    /**
     * Returns storage pids
     *
     * @return array
     */
    public function getStoragePids()
    {
        return $this->storagePids;
    }

    /**
     * Set storage pids
     *
     * @param array $storagePids storage pids
     * @return void
     */
    public function setStoragePids($storagePids)
    {
        $this->storagePids = $storagePids;
    }
}
