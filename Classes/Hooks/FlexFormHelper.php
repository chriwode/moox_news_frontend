<?php
namespace DCNGmbH\MooxNewsFrontend\Hooks;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *      2017 Christian Wolfram <c.wolfram@chriwo.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use DCNGmbH\MooxCommunity\Domain\Repository\FrontendUserGroupRepository;
use DCNGmbH\MooxNewsFrontend\Domain\Repository\ContentRepository;
use DCNGmbH\MooxNewsFrontend\Service\HelperService;
use DCNGmbH\MooxPayment\Service\PaymentService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Service\FlexFormService;
use TYPO3\CMS\Frontend\Page\PageRepository;

/**
 *
 *
 * @package moox_news_frontend
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class FlexFormHelper
{
    /**
     * objectManager
     *
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    protected $objectManager;
    
    /**
     * configurationManager
     *
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
     */
    protected $configurationManager;
    
    /**
     * flexFormService
     *
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    protected $flexFormService;
    
    /**
     * paymentService
     *
     * @var \DCNGmbH\MooxPayment\Service\PaymentService
     * @inject
     */
    protected $paymentService;
    
    /**
     * helperService
     *
     * @var \DCNGmbH\MooxNewsFrontend\Service\HelperService
     */
    protected $helperService;
    
    /**
     * pageRepository
     *
     * @var \TYPO3\CMS\Frontend\Page\PageRepository
     */
    protected $pageRepository;
    
    /**
     * contentRepository
     *
     * @var \DCNGmbH\MooxNewsFrontend\Domain\Repository\ContentRepository
     */
    protected $contentRepository;
    
    /**
     * frontendUserGroupRepository
     *
     * @var \DCNGmbH\MooxNewsFrontend\Domain\Repository\FrontendUserGroupRepository
     */
    protected $frontendUserGroupRepository;
    
    /**
     * configuration
     *
     * @var array
     */
    protected $configuration;
    
    /**
     * extConf
     *
     * @var array
     */
    protected $extConf;
    
    /**
     * Path to the locallang file
     * @var string
     */
    const LLPATH = 'LLL:EXT:moox_news_frontend/Resources/Private/Language/locallang_be.xlf:';
    
    /**
     * initialize action
     *
     * @return void
     */
    public function initialize()
    {
        $this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        $this->configurationManager = $this->objectManager->get(ConfigurationManagerInterface::class);
        $this->flexFormService = $this->objectManager->get(FlexFormService::class);
        $this->helperService = $this->objectManager->get(HelperService::class);
        $this->paymentService = $this->objectManager->get(PaymentService::class);
        $this->pageRepository = $this->objectManager->get(PageRepository::class);
        $this->contentRepository = $this->objectManager->get(ContentRepository::class);
        $this->frontendUserGroupRepository = $this->objectManager->get(FrontendUserGroupRepository::class);
        $this->configuration = $this->configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK,
            'MooxNewsFrontend'
        );
        $this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_news_frontend']);
    }
    
    /**
     * Itemsproc function to generate the selection of switchable controller actions
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function switchableControllerActions(array &$config, &$pObj)
    {
        $config['items'] = array();
        $actionsManageList = array('list', 'detail', 'add', 'edit', 'delete', 'visibility', 'privacy', 'error', 'payment');
        $actionsManage = array();

        foreach ($actionsManageList as $actionManage) {
            $actionsManage[] = 'Pi1->' . $actionManage;
        }

        $labelManage = $GLOBALS['LANG']->sL(self::LLPATH.'pi1.selection.my_news');
        $config['items'][] = array(0 => $labelManage, 1 => implode(';', $actionsManage));
        $config['config']['default'] = implode(';', $actionsManage);
    }
    
    /**
     * Itemsproc function to extend the selection of storage pid in flexform
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function storagePid(array &$config, &$pObj)
    {
        $this->initialize();

        // if valid persistence storage pid is set within typoscript setup
        if ($this->configuration['persistence']['storagePid'] != '') {
            // get page info for storage pid set by typoscript
            $page = $this->pageRepository->getPage($this->configuration['persistence']['storagePid']);
            $definedByTs = array(
                array(
                    '[Defined by TS]: ' . $page['title'] .
                    ' [PID: ' . $this->configuration['persistence']['storagePid'] . ']',
                    'TS'
                )
            );
        }
        
        // add pid postfix to item array element and remove invalid pids from array
        for ($i = 0; $i < count($config['items']); $i++) {
            if ($config['items'][$i][1] != '') {
                $config['items'][$i][0] = $config['items'][$i][0] . ' [PID: ' . $config['items'][$i][1] . ']';
            }
        }
        
        // if available add defined by ts value to items array
        if ($definedByTsTxt) {
            $config['items'] = array_merge($definedByTs, $config['items']);
        }
    }

    /**
     * Itemsproc function to extend the selection of storage pids in flexform
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function storagePids(array &$config, &$pObj)
    {
        // add pid postfix to item array element and remove invalid pids from array
        for ($i = 0; $i < count($config['items']); $i++) {
            if ($config['items'][$i][1] != '') {
                $config['items'][$i][0] = $config['items'][$i][0] . ' [PID: ' . $config['items'][$i][1] . ']';
            }
        }
    }
    
    /**
     * Itemsproc function to process the selection of fe groups in flexform
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function feGroups(array &$config, &$pObj)
    {
        // add pid postfix to item array element and remove invalid pids from array
        for ($i = 0; $i < count($config['items']); $i++) {
            if ($config['items'][$i][1] != '') {
                $config['items'][$i][0] = $config['items'][$i][0] . ' [PID: ' . $config['items'][$i][1] . ']';
            }
        }
    }
    
    /**
     * Itemsproc function to provide a selection of available payment methods
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function paymentMethods(array &$config, &$pObj)
    {
        // initialize
        $this->initialize();
        
        // init items array
        $config['items'] = array();
        
        if (is_object($this->paymentService)) {
            $paymentMethods = $this->paymentService->getAllPaymentMethods();
            if (count($paymentMethods)) {
                foreach ($paymentMethods as $method => $methodConfig) {
                    // add item
                    $config['items'][] = array($methodConfig['title'],$method);
                }
            } else {
                // add item
                $config['items'][] = array(
                    $GLOBALS['LANG']->sL(self::LLPATH . 'pi1.payment_methods.not_found', true),
                    ''
                );
            }
        } else {
            // add item
            $config['items'][] = array(
                $GLOBALS['LANG']->sL(self::LLPATH . 'pi1.payment_methods.not_installed', true),
                ''
            );
        }
    }
    
    /**
     * Itemsproc function to get a selection of available types for plugin pi1
     *
     * @param array &$config configuration array
     * @return void
     */
    public function pi1Types(array &$config)
    {
        // initialize
        $this->initialize();
        
        // init items array
        $config['items'] = array();
        
        // add item
        $config['items'][] = array($GLOBALS['LANG']->sL(self::LLPATH . 'pi1.type.userdefined', true), '');
        
        // get all types
        foreach ($this->helperService->getAvailableTypes() as $option) {
            $config['items'][] = array($option[0],$option[1]);
        }
    }
    
    /**
     * Itemsproc function to get a selection of available types for plugin pi2
     *
     * @param array &$config configuration array
     * @return void
     */
    public function pi2Types(array &$config)
    {
        // initialize
        $this->initialize();
        
        // init items array
        $config['items'] = array();
                
        // get all types
        foreach ($this->helperService->getAvailableTypes() as $option) {
            $config['items'][] = array($option[0], $option[1]);
        }
    }
    
    /**
     * Itemsproc function to get a selection of allowed types
     *
     * @param array &$config configuration array
     * @return void
     */
    public function allowedTypes(array &$config)
    {
        
        // initialize
        $this->initialize();
        
        // init items array
        $config['items'] = array();
        
        // get all types
        foreach ($this->helperService->getAvailableTypes() as $option) {
            $config['items'][] = array($option[0], $option[1]);
        }
    }
    
    /**
     * Itemsproc function to generate list of available fields
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @param string $action action
     * @param array $excludeFields exclude fields
     * @param bool $excludeHeader exclude header fields
     * @return array
     */
    public function fields(array &$config, &$pObj, $action = '', $excludeFields = array(), $excludeHeader = true)
    {
        $items = array();
        $sortable = 0;
        $llpath = 'LLL:EXT:moox_news/Resources/Private/Language/locallang.xlf:';

        $this->initialize();
        $news = $this->objectManager->get('DCNGmbH\\MooxNewsFrontend\\Domain\\Model\\News');
        
        // filter sort fields only
        if ($action == 'sort') {
            $action = 'list';
            $sortable = 1;
        }
        
        // get plugin fields
        $pluginFields = $this->helperService->getPluginFields(
            'tx_mooxnews_domain_model_news',
            'mooxnewsfrontend',
            $action
        );
        $pluginFieldGroups = $this->helperService->getPluginFieldGroups(
            'tx_mooxnews_domain_model_news',
            'mooxnewsfrontend',
            $action
        );
        
        // set translation lookup array
        if (in_array($action, array('add', 'edit'))) {
            $lookup = 'form';
        } else {
            $lookup = $action;
        }
        
        // get flex form data array
        if (in_array($action, array('add', 'edit'))) {
            $flexformData = $this->flexFormService->convertFlexFormContentToArray($config['row']['pi_flexform']);
            if ($flexformData['settings']['type'] != '') {
                $excludeFields[] = 'type';
                $type = $flexformData['settings']['type'];
            }
        }
        
        // add valid fields to items array
        foreach ($pluginFields as $fieldname => $field) {
            if ((!$excludeHeader || !$field['moox']['header'])
                && !in_array($fieldname, $excludeFields)
                && (!$sortable || $field['moox']['sortable'])
                && ($type == '' || $field['moox']['extkey'] == 'moox_news_frontend' ||
                    $field['moox']['extkey'] == $type || $field['moox']['type']==$type)
            ) {
                // check if field has valid setter/getter method
                $setMethod = 'set'.GeneralUtility::underscoredToUpperCamelCase($fieldname);
                $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($fieldname);

                if ((in_array($action, array('add', 'edit')) && method_exists($news, $setMethod))
                    || (in_array($action, array('list', 'detail')) && method_exists($news, $getMethod))
                    || (!$excludeHeader && $field['moox']['header'])
                ) {
                    $label = $GLOBALS['LANG']->sL($llpath . $lookup . '.' . $fieldname);
                    if ($label == '') {
                        $label = $GLOBALS['LANG']->sL($llpath . 'form.' . $fieldname);
                    }

                    if ($label == '') {
                        $label = $GLOBALS['LANG']->sL($field['label']);
                    }

                    if ($label == '') {
                        $label = $fieldname;
                    }

                    if ($field['moox']['header']) {
                        $label = '[' . $GLOBALS['LANG']->sL($llpath . 'header') . '] ' . $label;
                    } elseif ($field['moox']['extkey'] != 'moox_news') {
                        $label = '[' . $field['moox']['extkey'] . '] ' . $label;
                    }

                    $items[] = array(0 => $label, 1 => $fieldname);
                }
            }
        }
        
        // add valid groups to items array
        foreach ($pluginFieldGroups as $group) {
            $items[] = array(
                0 => '[' . $GLOBALS['LANG']->sL($llpath . 'group') . '] ' . $GLOBALS['LANG']->sL($group[0]),
                1 => $group[1]
            );
        }
        
        return $items;
    }
    
    /**
     * Itemsproc function to generate list of available list fields
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function listFields(array &$config, &$pObj)
    {
        $llpath = 'LLL:EXT:moox_news_frontend/Resources/Private/Language/locallang.xlf:';
        $config['items'] = array();
        $config['items'] = $this->fields($config, $pObj, 'list');
        $config['items'][] = array($GLOBALS['LANG']->sL($llpath . 'list.summary'), 'summary');
    }
    
    /**
     * Itemsproc function to generate list of available detail fields
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function detailFields(array &$config, &$pObj)
    {
        $config['items'] = array();
        $config['items'] = $this->fields($config, $pObj, 'detail');
    }
    
    /**
     * Itemsproc function to generate list of available private detail fields
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function privateDetailFields(array &$config, &$pObj)
    {
        $config['items'] = array();
        $config['items'] = $this->fields($config, $pObj, 'detail');
    }
    
    /**
     * Itemsproc function to generate list of available add fields
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function addFields(array &$config, &$pObj)
    {
        $config['items'] = array();
        $config['items'] = $this->fields($config, $pObj, 'add', array(), false);
    }
    
    /**
     * Itemsproc function to generate list of available edit fields
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function editFields(array &$config, &$pObj)
    {
        $config['items'] = array();
        $config['items'] = $this->fields($config, $pObj, 'edit', array(), false);
    }
    
    /**
     * Itemsproc function to generate list of available add fields (required)
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function requiredAddFields(array &$config, &$pObj)
    {
        $config['items'] = array();
        $config['items'] = $this->fields($config, $pObj, 'add');
    }
    
    /**
     * Itemsproc function to generate list of available edit fields (required)
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function requiredEditFields(array &$config, &$pObj)
    {
        $config['items'] = array();
        $config['items'] = $this->fields($config, $pObj, 'edit');
    }

    /**
     * Itemsproc function to generate list of available order fields
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function orderBy(array &$config, &$pObj)
    {
        $config['items'] = array();
        $config['items'][] = array($GLOBALS['LANG']->sL(self::LLPATH.'pi1.order_by.userdefined', true),'');

        foreach ($this->fields($config, $pObj, 'sort') as $option) {
            $config['items'][] = array($option[0],$option[1]);
        }
    }
    
    /**
     * Itemsproc function to generate list of allowed order fields
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function allowedOrderBy(array &$config, &$pObj)
    {
        $config['items'] = array();
        $config['items'] = $this->fields($config, $pObj, 'sort');
    }
}
