<?php
namespace DCNGmbH\MooxNewsFrontend\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *      2017 Christian Wolfram <c.wolfram@chriwo.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 *
 *
 * @package moox_news_frontend
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Pi2Controller extends BaseController
{
    /**
     * membershipRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\MembershipRepository
     * @inject
     */
    protected $membershipRepository;

    /**
     * friendshipRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\FriendshipRepository
     * @inject
     */
    protected $friendshipRepository;

    /**
     * categories
     *
     * @var array
     */
    protected $categories;

    /**
     * type
     *
     * @var array
     */
    protected $types;

    /**
     * initialize action
     *
     * @return void
     */
    public function initializeAction()
    {
        if ($this->settings['categories'] != '') {
            $this->categories = GeneralUtility::trimExplode(',', $this->settings['categories']);
        } else {
            $this->categories = null;
        }

        if ($this->settings['types'] != '') {
            $this->types = GeneralUtility::trimExplode(',', $this->settings['types']);
        } else {
            $this->types = array();
        }

        parent::initializeAction();
    }

    /**
     * action list
     *
     * @param array $filter
     * @return void
     */
    public function listAction($filter = null)
    {
        if ($this->feUser) {
            $order['by'] = key($this->orderings);
            $order['dir'] = current($this->orderings);

            // get order by fields set by plugin
            $orderByFields = $this->helperService->configureFields(
                $this->allowedOrderBy,
                '',
                $this->settings,
                $this->fieldConfig
            );

            // get groups if set in plugin options
            $groups = array();
            $groups[] = 0;
            if ($this->settings['loadGroupNews']) {
                $memberships = $this->membershipRepository->findByFilter(
                    array('feUser' => $this->feUser, 'confirmed' => 1),
                    null,
                    null,
                    null,
                    'all'
                );
                
                foreach ($memberships as $membership) {
                    $group = $membership->getFeGroup();
                    if (is_object($group)) {
                        $groups[] = $group->getUid();
                    }
                }
            }

            // get friends if set in plugin options
            $friends = array();
            $friends[] = 0;
            if ($this->settings['loadFriendNews']) {
                $friendships = $this->friendshipRepository->findByFilter(
                    array('friend' => $this->feUser, 'confirmed' => 1),
                    null,
                    null,
                    null,
                    'all'
                );
                
                foreach ($friendships as $friendship) {
                    if ($friendship->getFeUser2()->getUid() == $this->feUser->getUid()) {
                        $friend = $friendship->getFeUser1();
                    } else {
                        $friend = $friendship->getFeUser2();
                    }
                    
                    if (is_object($friend)) {
                        $friends[] = $friend->getUid();
                    }
                }
            }

            // get own news if set in plugin options
            if ($this->settings['loadOwnNews']) {
                $filter['own'] = $this->feUser;
            } else {
                $filter['foreign'] = $this->feUser;
            }

            // set fe users and fe groups filter
            $filter['feCrusers'] = $friends;
            $filter['feCrgroups'] = $groups;

            // set privacy filter
            if ($this->settings['loadPublicNews'] && !$this->settings['loadPrivateNews']) {
                $filter['privacy'] = 0;
            } elseif ($this->settings['loadPrivateNews'] && !$this->settings['loadPublicNews']) {
                $filter['privacy'] = 1;
            }

            // set categories filter
            if ($this->categories) {
                $filter['categories'] = $this->categories;
            }

            // set type filter
            if (count($this->types)) {
                $filter['types'] = $this->types;
            }

            // get items
            $items = $this->newsRepository->findByFilter(
                $filter,
                $this->orderings,
                null,
                null,
                (count($this->storagePids)) ? $this->storagePids : 'all',
                null
            );

            $assignMultiple = array(
                'filter' => $filter,
                'order' => $order,
                'orderByFields' => $orderByFields,
                'pagination' => $this->pagination,
                'items' => $items,
                'returnUrl' => $this->uriBuilder->getRequest()->getRequestUri(),
                'action' => 'list'
            );
            $this->view->assignMultiple($assignMultiple);
        }
    }

    /**
     * action detail
     *
     * @param int $item
     * @param string $returnUrl
     * @return void
     */
    public function detailAction($item = null, $returnUrl = null)
    {
        $messages = array();
        $readable = false;
        $isOwner = false;
        $isMember = false;
        $isFriend = false;
        $showPrivate = false;

        // check and get logged in user
        if ($item > 0) {
            $item = $this->newsRepository->findByUid($item);

            if (is_object($item) && !is_null($this->feUser)) {
                // get groups if set in plugin options
                $groups = array();
                if ($this->settings['loadGroupNews']) {
                    $memberships = $this->membershipRepository->findByFilter(
                        array('feUser' => $this->feUser, 'confirmed' => 1),
                        null,
                        null,
                        null,
                        'all'
                    );
                    
                    foreach ($memberships as $membership) {
                        $group = $membership->getFeGroup();
                        if (is_object($group)) {
                            $groups[] = $group->getUid();
                        }
                    }
                }

                // get friends if set in plugin options
                $friends = array();
                if ($this->settings['loadFriendNews']) {
                    $friendships = $this->friendshipRepository->findByFilter(
                        array('friend' => $this->feUser, 'confirmed' => 1),
                        null,
                        null,
                        null,
                        'all'
                    );
                    
                    foreach ($friendships as $friendship) {
                        if ($friendship->getFeUser2()->getUid() == $this->feUser->getUid()) {
                            $friend = $friendship->getFeUser1();
                        } else {
                            $friend = $friendship->getFeUser2();
                        }
                        if (is_object($friend)) {
                            $friends[] = $friend->getUid();
                        }
                    }
                }
                
                if (is_object($item->getFeCruser()) && $item->getFeCruser()->getUid() == $this->feUser->getUid()) {
                    $isOwner = true;
                    $showPrivate = true;
                }
                
                if (is_object($item->getFeCruser())
                    && (in_array($item->getFeCruser()->getUid(), $friends)
                        || (is_object($item->getFeCrgroup()) && in_array($item->getFeCrgroup()->getUid(), $groups)))
                ) {
                    if (is_object($item->getFeCrgroup()) && in_array($item->getFeCrgroup()->getUid(), $groups)
                    ) {
                        $isMember = true;
                        $showPrivate = true;
                    }
                    
                    if (in_array($item->getFeCruser()->getUid(), $friends)) {
                        $isFriend = true;
                        $showPrivate = true;
                    }
                }
                
                if (($item->getPrivacy() == 1 && $showPrivate) || $item->getPrivacy() == 0) {
                    $readable = true;
                }
            }
        }

        // if item is readable by current user
        if (!$readable) {
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH . 'pi2.action_detail', $this->extensionName),
                'text' => LocalizationUtility::translate(
                    self::LLPATH . 'pi2.action_detail.error.no_access',
                    $this->extensionName
                ),
                'type' => FlashMessage::ERROR
            );
            $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
            $this->redirect('error');
        }
        
        // get fields set by plugin
        $fields = $this->helperService->configureFields(
            $this->settings['detailFields'],
            '',
            $this->settings,
            $this->fieldConfig,
            $item->getType()
        );

        // get private fields set by plugin
        $privateFields = $this->helperService->configureFields(
            $this->settings['privateDetailFields'],
            '',
            $this->settings,
            $this->fieldConfig,
            $item->getType()
        );

        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);

        $assignMultiple = array(
            'item' => $item,
            'fields' => $fields,
            'privateFields' => $privateFields,
            'readable' => $readable,
            'showPrivate' => $showPrivate,
            'isOwner' => $isOwner,
            'isMember' => $isMember,
            'isFriend' => $isFriend,
            'returnUrl' => $returnUrl,
            'action' => 'detail'
        );
        $this->view->assignMultiple($assignMultiple);
    }

    /**
     * action error
     *
     * @return void
     */
    public function errorAction()
    {
        $this->view->assign('action', 'error');
    }

    /**
     * Returns fields
     *
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Set fields
     *
     * @param array $fields fields
     * @return void
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
    }

    /**
     * Returns fieldConfig
     *
     * @return array
     */
    public function getFieldConfig()
    {
        return $this->fieldConfig;
    }

    /**
     * Set fieldConfig
     *
     * @param array $fieldConfig fieldConfig
     * @return void
     */
    public function setFieldConfig($fieldConfig)
    {
        $this->fieldConfig = $fieldConfig;
    }

    /**
     * Returns ext conf
     *
     * @return array
     */
    public function getExtConf()
    {
        return $this->extConf;
    }

    /**
     * Set ext conf
     *
     * @param array $extConf ext conf
     * @return void
     */
    public function setExtConf($extConf)
    {
        $this->extConf = $extConf;
    }
}
