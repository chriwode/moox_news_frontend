<?php
namespace DCNGmbH\MooxNewsFrontend\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Christian Wolfram <c.wolfram@chriwo.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 * Class BaseController
 *
 * @package moox_news_frontend
 */
class BaseController extends ActionController
{
    /**
     * accessControllService
     *
     * @var \DCNGmbH\MooxNewsFrontend\Service\AccessControlService
     * @inject
     */
    protected $accessControllService;

    /**
     * helperService
     *
     * @var \DCNGmbH\MooxNewsFrontend\Service\HelperService
     * @inject
     */
    protected $helperService;

    /**
     * frontendUserRepository
     *
     * @var \DCNGmbH\MooxNewsFrontend\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $frontendUserRepository;

    /**
     * newsRepository
     *
     * @var \DCNGmbH\MooxNewsFrontend\Domain\Repository\NewsRepository
     * @inject
     */
    protected $newsRepository;

    /**
     * categoryRepository
     *
     * @var \DCNGmbH\MooxNewsFrontend\Domain\Repository\CategoryRepository
     * @inject
     */
    protected $categoryRepository;

    /**
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\AboMembershipRepository
     * @inject
     */
    protected $aboRepository;

    /**
     * extConf
     *
     * @var array
     */
    protected $extConf;

    /**
     * storagePids
     *
     * @var array
     */
    protected $storagePids;

    /**
     * fields
     *
     * @var array
     */
    protected $fields;

    /**
     * fieldConfig
     *
     * @var array
     */
    protected $fieldConfig;

    /**
     * pagination
     *
     * @var array
     */
    protected $pagination;

    /**
     * orderings
     *
     * @var array
     */
    protected $orderings;

    /**
     * allowedOrderBy
     *
     * @var string
     */
    protected $allowedOrderBy;

    /**
     * @var object
     */
    protected $feUser = null;

    /**
     * @var array
     */
    protected $typoScriptConfiguration;

    /**
     * Path to the locallang file
     *
     * @var string
     */
    const LLPATH = 'LLL:EXT:moox_news_frontend/Resources/Private/Language/locallang.xlf:';

    /**
     * initialize action
     *
     * @return void
     * @SuppressWarnings(PHPMD.Superglobals)
     */
    public function initializeAction()
    {
        parent::initializeAction();
        $this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_news_frontend']);

        if ($this->settings['itemsPerPage'] > 0) {
            $this->pagination['itemsPerPage'] = $this->settings['itemsPerPage'];
        } else {
            $this->pagination['itemsPerPage'] = 10000;
        }

        if ($this->settings['orderBy'] != '') {
            $orderBy = $this->settings['orderBy'];
            $this->allowedOrderBy = $orderBy;
        } else {
            if ($this->settings['allowedOrderBy'] != '') {
                $allowedOrderBy = GeneralUtility::trimExplode(',', $this->settings['allowedOrderBy']);
                $orderBy = $allowedOrderBy[0];
                $this->allowedOrderBy = $this->settings['allowedOrderBy'];
            } else {
                $orderBy = 'crdate';
                $this->allowedOrderBy = $orderBy;
            }
        }

        if ($this->settings['orderDirection'] != '') {
            $orderDirection = $this->settings['orderDirection'];
        } else {
            $orderDirection = 'ASC';
        }
        $this->orderings = array($orderBy => $orderDirection);

        $this->fields = $this->helperService->getPluginFields(
            'tx_mooxnews_domain_model_news',
            'mooxnewsfrontend',
            'all'
        );

        $this->typoScriptConfiguration = $this->configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK
        );
        $this->initializeStorageSettings($this->typoScriptConfiguration);
        $this->fieldConfig = $this->helperService->getFieldConfig($this->fields, $this->settings);

        // check and get logged in user
        if (true === $this->accessControllService->hasLoggedInFrontendUser()) {
            $this->feUser = $this->frontendUserRepository->findByUid(
                $this->accessControllService->getFrontendUserUid()
            );
        }
    }

    /**
     * initialize storage settings
     *
     * @param array $configuration
     * @return void
     */
    protected function initializeStorageSettings($configuration = [])
    {

        // set storage pid if set by plugin
        if ($this->settings['storagePids'] != '') {
            $this->setStoragePids(GeneralUtility::intExplode(',', $this->settings['storagePids']));
        } else {
            $this->setStoragePids(array());
        }
        $this->helperService->setStoragePids($this->getStoragePids());

        // overwrite extbase persistence storage pid
        if (!empty($this->settings['storagePid']) && $this->settings['storagePid'] != 'TS') {
            if (empty($configuration['persistence']['storagePid'])) {
                $storagePids['persistence']['storagePid'] = $this->settings['storagePid'];
                $this->configurationManager->setConfiguration(array_merge($configuration, $storagePids));
            } else {
                $configuration['persistence']['storagePid'] = $this->settings['storagePid'];
                $this->configurationManager->setConfiguration($configuration);
            }
        }
    }

    /**
     * Returns storage pids
     *
     * @return array
     */
    public function getStoragePids()
    {
        return $this->storagePids;
    }

    /**
     * Set storage pids
     *
     * @param array $storagePids storage pids
     * @return void
     */
    public function setStoragePids($storagePids)
    {
        $this->storagePids = $storagePids;
    }
}
