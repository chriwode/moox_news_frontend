<?php
namespace DCNGmbH\MooxNewsFrontend\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *      2017 Christian Wolfram <c.wolfram@chriwo.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use DCNGmbH\MooxNewsFrontend\Hooks\Tcemain;
use DCNGmbH\MooxPayment\Service\PaymentService;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 *
 *
 * @package moox_news_frontend
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Pi1Controller extends BaseController
{
    /**
     * persistenceManager
     *
     * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface
     * @inject
     */
    protected $persistenceManager;

    /**
     * frontendUserGroupRepository
     *
     * @var \DCNGmbH\MooxNewsFrontend\Domain\Repository\FrontendUserGroupRepository
     * @inject
     */
    protected $frontendUserGroupRepository;

    /**
     * storageRepository
     *
     * @var \TYPO3\CMS\Core\Resource\StorageRepository
     * @inject
     */
    protected $storageRepository;

    /**
     * fileRepository
     *
     * @var \DCNGmbH\MooxNewsFrontend\Domain\Repository\FileRepository
     * @inject
     */
    protected $fileRepository;

    /**
     * fileReferenceRepository
     *
     * @var \DCNGmbH\MooxNewsFrontend\Domain\Repository\FileReferenceRepository
     * @inject
     */
    protected $fileReferenceRepository;

    /**
     * paymentService
     *
     * @var \DCNGmbH\MooxPayment\Service\PaymentService
     * @inject
     */
    protected $paymentService;

    /**
     * fileStorageUid
     *
     * @var int
     */
    protected $fileStorageUid;

    /**
     * fileStorageFolder
     *
     * @var int
     */
    protected $fileStorageFolder;

    /**
     * type
     *
     * @var string
     */
    protected $type;

    /**
     * allowedTypes
     *
     * @var array
     */
    protected $allowedTypes;

    /**
     * partialRootPath
     *
     * @var string
     */
    protected $partialRootPath;

    /**
     * fe user type
     *
     * @var string
     */
    const TYPE = 'moox_news_frontend';

    /**
     * temp folder
     *
     * @var string
     */
    const TEMPFOLDER = 'moox_news_frontend';

    /**
     * initialize action
     *
     * @return void
     */
    public function initializeAction()
    {
        parent::initializeAction();

        $this->settings['formKey'] = 'tx_mooxnewsfrontend_pi1';

        if (ExtensionManagementUtility::isLoaded('moox_payment')) {
            $this->paymentService = $this->objectManager->get(PaymentService::class);
        }

        // set file storage uid
        if ($this->settings['fileStorage'] != '') {
            $this->fileStorageUid = $this->settings['fileStorage'];
        } else {
            $this->fileStorageUid = 1;
        }

        // set file storage folder
        if ($this->settings['fileStorageFolder'] != '') {
            $this->fileStorageFolder = $this->settings['fileStorageFolder'];
        } else {
            $this->fileStorageFolder = 'moox_news_frontend';
        }

        // set type
        if ($this->settings['type'] != '') {
            $this->type = $this->settings['type'];
            $this->allowedTypes = array($this->type => GeneralUtility::underscoredToUpperCamelCase($this->type));
        } else {
            if ($this->settings['allowedTypes'] != '') {
                $this->allowedTypes = array();
                $allowedTypes = explode(',', $this->settings['allowedTypes']);
                foreach ($allowedTypes as $type) {
                    $this->allowedTypes[$type] = GeneralUtility::underscoredToUpperCamelCase($type);
                }
            } else {
                $this->allowedTypes = array();
            }
            $this->type = null;
        }

        $this->partialRootPath = end($this->typoScriptConfiguration['view']['partialRootPaths']);
    }

    /**
     * action list
     *
     * @param array $filter
     * @return void
     */
    public function listAction($filter = null)
    {
        $messages = array();

        if (is_null($this->feUser)) {
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_list', $this->extensionName),
                'text' => LocalizationUtility::translate(
                    self::LLPATH . 'pi1.action_list.error.no_access',
                    $this->extensionName
                ),
                'type' => FlashMessage::ERROR
            );
            
            $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
            $this->redirect('error');
        }
        
        $order['by'] = key($this->orderings);
        $order['dir'] = current($this->orderings);

        // get fields set by plugin
        $fields = $this->helperService->configureFields(
            $this->settings['listFields'],
            '',
            $this->settings,
            $this->fieldConfig
        );

        // get order by fields set by plugin
        $orderByFields = $this->helperService->configureFields(
            $this->allowedOrderBy,
            '',
            $this->settings,
            $this->fieldConfig
        );

        // set fe user filter
        $filter['feUser'] = $this->feUser;

        // set type filter
        if ($this->type) {
            $filter['type'] = $this->type;
        } elseif ($this->allowedTypes) {
            $filter['types'] = array();
            foreach ($this->allowedTypes as $type => $label) {
                $filter['types'][] = $type;
            }
        }

        // get items
        $items = $this->newsRepository->findByFilter(
            $filter,
            $this->orderings,
            null,
            null,
            $this->storagePids,
            array('disabled')
        );

        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);

        $multipleAssign = array(
            'fields' => $fields,
            'filter' => $filter,
            'order' => $order,
            'orderByFields' => $orderByFields,
            'pagination' => $this->pagination,
            'items' => $items,
            'allowedTypes' => $this->allowedTypes,
            'returnUrl' => $this->uriBuilder->getRequest()->getRequestUri(),
            'action' => 'list'
        );
        $this->view->assignMultiple($multipleAssign);
    }

    /**
     * action detail
     *
     * @param int $item
     * @param string $returnUrl
     * @return void
     */
    public function detailAction($item = null, $returnUrl = null)
    {
        $messages = array();
        $readable = false;

        if ($item > 0) {
            $item = $this->newsRepository->findByUid($item, false);
        }
        
        // if user is logged in and correct item is passed
        if ($this->feUser && is_a($item, '\DCNGmbH\MooxNewsFrontend\Domain\Model\News')) {
            if ($this->feUser->getUid() == $item->getFeCruser()->getUid()) {
                $readable = true;
            }
        }

        if (!$readable) {
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_detail', $this->extensionName),
                'text' => LocalizationUtility::translate(
                    self::LLPATH . 'pi1.action_detail.error.no_access',
                    $this->extensionName
                ),
                'type' => FlashMessage::ERROR
            );

            $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
            $this->redirect('error');
        }
        
        $fields = $this->helperService->configureFields(
            $this->settings['detailFields'],
            '',
            $this->settings,
            $this->fieldConfig,
            $item->getType()
        );

        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);

        $multipleAssign = array(
            'item' => $item,
            'fields' => $fields,
            'returnUrl' => $returnUrl,
            'action' => 'detail'
        );
        $this->view->assignMultiple($multipleAssign);
    }

    /**
     * action add
     *
     * @param array $add
     * @param string $type
     * @param array $payment
     * @param string $returnUrl
     * @param bool $reload
     * @return void
     */
    public function addAction($add = null, $type = null, $payment = null, $returnUrl = null, $reload = false)
    {
        $messages = array();
        $errors = array();
        $fileAddOnly = false;
        $fileRemoveOnly = false;
        $paymentMethods = null;

        // set type
        if (isset($add['type']) && $add['type'] != '') {
            $type = $add['type'];
        } elseif ($type != ''
            && in_array(GeneralUtility::underscoredToUpperCamelCase($type), $this->allowedTypes)
            && (!$this->type || in_array($type, GeneralUtility::trimExplode(',', $this->type)))
        ) {
            $type = $type;
        } elseif ($this->type) {
            $type = $this->type;
        } else {
            $type = GeneralUtility::camelCaseToLowerCaseUnderscored($this->allowedTypes[0]);
        }

        if (is_null($this->feUser) && empty($type)) {
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_add', $this->extensionName),
                'text' => LocalizationUtility::translate(
                    self::LLPATH . 'pi1.action_add.error.no_access',
                    $this->extensionName
                ),
                'type' => FlashMessage::ERROR
            );

            $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
            $this->redirect('error');
        }

        $paymentRequired = $this->accessControllService->checkPaymentStatus($this->feUser, $type);
        if ($paymentRequired) {
            $paymentMethods = $this->paymentService->getPaymentMethods($this->settings['paymentMethods']);
        }

        $fields = $this->helperService->configureFields(
            $this->settings['addFields'],
            $this->settings['requiredAddFields'],
            $this->settings,
            $this->fieldConfig,
            $type,
            $this->allowedTypes
        );

        // process form values
        if ($add && !$reload) {
            // add or remove temp files by add button
            foreach ($fields as $field) {
                // if field is a file field
                if ($field['config']['type'] == 'file') {
                    // if temporary file form fields are set
                    if (is_array($add[$field['key'] . '_tmp'])) {
                        // go through all temporary file form fields
                        foreach ($add[$field['key'] . '_tmp'] as $key => $value) {
                            // if remove button for this temporary field is set
                            if (isset($add[$field['key'] . '_remove_' . $key])) {
                                // remove this temporary file form field from form
                                unset($add[$field['key'] . '_tmp'][$key]);

                                // add message
                                $messages[] = array(
                                    'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                                    'title' => LocalizationUtility::translate(
                                        self::LLPATH . 'pi1.action_add',
                                        $this->extensionName
                                    ),
                                    'text' => LocalizationUtility::translate(
                                        self::LLPATH . 'pi1.action_all.removed.' . $field['config']['reference-type'],
                                        $this->extensionName
                                    ),
                                    'type' => FlashMessage::OK
                                );

                                // set "file remove only" to prevent further processing of form values
                                $fileRemoveOnly = true;
                            }
                        }
                    }

                    // if form file add button was pressed for this field
                    if (isset($add[$field['key']]) && isset($add[$field['key'] . '_add'])) {
                        // if file is selected in this form file field
                        if ($add[$field['key']]['name'] != '') {
                            // get current count of temporary file form fields for this file field
                            $fileCnt = (is_array($add[$field['key'] . '_tmp'])) ? count($add[$field['key'] . '_tmp']
                            ) : 0;

                            // if maxitems not reached yet for this field
                            if (!isset($field['config']['maxitems']) || (isset($field['config']['maxitems'])
                                    && $fileCnt < $field['config']['maxitems'])
                            ) {
                                // get file information for selected file
                                $fileInfo = $this->helperService->getFileInfo($add[$field['key']]['name']);

                                // set file name to store
                                $fileName = $fileInfo['name'] . '.' . $fileInfo['extension'];

                                // make temporary copy of file in typo3 tempfolder
                                $fileTemp = $this->helperService->copyToTemp(
                                    $add[$field['key']]['tmp_name'],
                                    $fileName,
                                    self::TEMPFOLDER
                                );

                                // set temporary file form fields for added file
                                $add[$field['key'] . '_tmp'][$fileCnt]['name'] = $fileName;
                                $add[$field['key']]['tmp_name_local'] = $fileTemp['path'];
                                $add[$field['key'] . '_tmp'][$fileCnt]['src'] = $fileTemp['src'];
                                $add[$field['key'] . '_tmp'][$fileCnt]['ext'] = $fileInfo['extension'];
                                $add[$field['key'] . '_tmp'][$fileCnt]['size'] = $add[$field['key']]['size'];
                                $add[$field['key'] . '_tmp'][$fileCnt]['object'] = serialize($add[$field['key']]);

                                // add message
                                $messages[] = array(
                                    'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                                    'title' => LocalizationUtility::translate(
                                        self::LLPATH . 'pi1.action_add',
                                        $this->extensionName
                                    ),
                                    'text' => LocalizationUtility::translate(
                                        self::LLPATH . 'pi1.action_all.added.' . $field['config']['reference-type'],
                                        $this->extensionName
                                    ),
                                    'type' => FlashMessage::OK
                                );
                            } else {
                                // add message
                                $message = LocalizationUtility::translate(
                                    self::LLPATH . 'form.' . $field['key'] . '.error.too_many',
                                    $this->extensionName,
                                    array($field['config']['maxitems'])
                                );
                                if ($message == '') {
                                    $message = LocalizationUtility::translate(
                                        str_replace('moox_news_frontend', $field['extkey'], self::LLPATH)
                                        . 'form.' . $field['key'] . '.error.too_many',
                                        $field['extkey'],
                                        array($field['config']['maxitems'])
                                    );
                                }
                                if ($message == '') {
                                    $message = LocalizationUtility::translate(
                                        self::LLPATH . 'form.error.too_many',
                                        $this->extensionName,
                                        array($field['config']['maxitems'])
                                    );
                                }
                                $title = LocalizationUtility::translate(
                                    self::LLPATH . 'form.' . $field['key'],
                                    $this->extensionName
                                );
                                if ($title == '') {
                                    $title = LocalizationUtility::translate(
                                        str_replace('moox_news_frontend', $field['extkey'], self::LLPATH)
                                        . 'form.' . $field['key'],
                                        $field['extkey']
                                    );
                                }
                                $messages[] = array(
                                    'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                                    'title' => $title,
                                    'text' => $message,
                                    'type' => FlashMessage::ERROR
                                );

                                // set error
                                $errors[$field['key']] = true;
                            }
                        } else {
                            // add message
                            $message = LocalizationUtility::translate(
                                self::LLPATH . 'form.' . $field['key'] . '.error.empty',
                                $this->extensionName
                            );
                            if ($message == '') {
                                $message = LocalizationUtility::translate(
                                    str_replace('moox_news_frontend', $field['extkey'], self::LLPATH)
                                    . 'form.' . $field['key'] . '.error.empty',
                                    $field['extkey']
                                );
                            }
                            if ($message == '') {
                                $message = LocalizationUtility::translate(
                                    self::LLPATH . 'form.error.empty',
                                    $this->extensionName
                                );
                            }
                            $title = LocalizationUtility::translate(
                                self::LLPATH . 'form.' . $field['key'],
                                $this->extensionName
                            );
                            if ($title == '') {
                                $title = LocalizationUtility::translate(
                                    str_replace('moox_news_frontend', $field['extkey'], self::LLPATH)
                                    . 'form.' . $field['key'],
                                    $field['extkey']
                                );
                            }
                            $messages[] = array(
                                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                                'title' => $title,
                                'text' => $message,
                                'type' => FlashMessage::ERROR
                            );

                            // set error
                            $errors[$field['key']] = true;
                        }

                        // set "file add only" to prevent further processing of form values
                        $fileAddOnly = true;
                    }
                }
            }

            // if general save button is pressed
            if (!$fileAddOnly && !$fileRemoveOnly) {
                $this->helperService->checkFields($fields, $add, $messages, $errors);

                // check payment if needed
                if ($paymentRequired) {
                    $paid = $this->helperService->checkPayment($payment, $messages, $errors);
                }

                // set item values
                $item = $this->objectManager->get('DCNGmbH\MooxNewsFrontend\Domain\Model\News');
                foreach ($fields as $field) {
                    if (!$field['passhrough']
                        && !in_array($field['config']['type'], array('file'))
                        && !in_array($field['key'], array('type', 'categories', 'fe_crgroup'))
                    ) {
                        $setMethod = 'set' . GeneralUtility::underscoredToUpperCamelCase($field['key']);
                        if (method_exists($item, $setMethod) && isset($add[$field['key']])) {
                            if (in_array($field['config']['type'], array('date'))) {
                                $item->$setMethod($this->helperService->getTimestamp($add[$field['key']]));
                            } elseif (in_array($field['config']['type'], array('datetime'))) {
                                $item->$setMethod(
                                    $this->helperService->getTimestamp(
                                        $add[$field['key']],
                                        $add[$field['key'] . '_hh'],
                                        $add[$field['key'] . '_mm']
                                    )
                                );
                            } else {
                                $item->$setMethod(
                                    (
                                        is_array($add[$field['key']])) ? implode(',', $add[$field['key']]) : trim($add[$field['key']]
                                    )
                                );
                            }
                        }
                    }
                }

                // no errors -> add news
                if (!count($errors)) {
                    // set item type
                    $item->setType($type);

                    // set current datetime, if no datetime is set
                    if (!isset($add['datetime']) && !$item->getDatetime()) {
                        $item->setDatetime(time());
                    }

                    // set file relations
                    foreach ($fields as $field) {
                        // if field is a file field
                        if (in_array($field['config']['type'], array('file'))) {
                            // if file is selected in this form file field
                            if ($add[$field['key']]['name'] != '') {
                                // get current count of temporary file form fields for this file field
                                $fileCnt = (is_array($add[$field['key'] . '_tmp'])) ? count($add[$field['key'] . '_tmp']) : 0;

                                // get file information for selected file
                                $fileInfo = $this->helperService->getFileInfo($add[$field['key']]['name']);

                                // set file name to store
                                $fileName = $fileInfo['name'] . '.' . $fileInfo['extension'];

                                // make temporary copy of file in typo3 tempfolder
                                $fileTemp = $this->helperService->copyToTemp(
                                    $add[$field['key']]['tmp_name'],
                                    $fileName,
                                    self::TEMPFOLDER
                                );

                                // set temporary file form fields for added file
                                $add[$field['key'] . '_tmp'][$fileCnt]['name'] = $fileName;
                                $add[$field['key']]['tmp_name_local'] = $fileTemp['path'];
                                $add[$field['key'] . '_tmp'][$fileCnt]['src'] = $fileTemp['src'];
                                $add[$field['key'] . '_tmp'][$fileCnt]['ext'] = $fileInfo['extension'];
                                $add[$field['key'] . '_tmp'][$fileCnt]['size'] = $add[$field['key']]['size'];
                                $add[$field['key'] . '_tmp'][$fileCnt]['object'] = serialize($add[$field['key']]);
                            }

                            // if any temporary files are selected for this field
                            if (isset($add[$field['key'] . '_tmp'])
                                && is_array($add[$field['key'] . '_tmp'])
                                && count($add[$field['key'] . '_tmp'])
                            ) {
                                // prepare get method call
                                $getMethod = 'get' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                                // prepare add method call
                                $addMethod = 'add' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                                // if get and add methods are existing
                                if (method_exists($item, $getMethod) && method_exists($item, $addMethod)) {
                                    // get file storage
                                    $storage = $this->storageRepository->findByUid($this->fileStorageUid);

                                    // create file storage folder if not already existing
                                    $folder = $storage->createFolder($this->fileStorageFolder);

                                    // go through all temporary files
                                    foreach ($add[$field['key'] . '_tmp'] as $tmpFile) {
                                        // get real file form object from temporary file
                                        $tmpFile['object'] = unserialize($tmpFile['object']);

                                        // add file to storage
                                        $fileObject = $storage->addFile(
                                            $tmpFile['object']['tmp_name_local'],
                                            $folder,
                                            $tmpFile['object']['name']
                                        );

                                        // get recently added file object from storage
                                        $fileObject = $storage->getFile($fileObject->getIdentifier());

                                        // get real file object from file repo
                                        $file = $this->fileRepository->findByUid($fileObject->getProperty('uid'));

                                        // set reference type depending on field
                                        if ($field['config']['reference-type'] == 'image') {
                                            $referenceType = 'ImageReference';
                                        } else {
                                            $referenceType = 'FileReference';
                                        }

                                        // set reference extkey
                                        if ($field['config']['extkeyUCC'] && $field['config']['extkeyUCC'] != 'MooxNewsFrontend' && $field['config']['extkeyUCC'] != 'MooxNews') {
                                            $referenceExtkey = $field['config']['extkeyUCC'];
                                        } else {
                                            $referenceExtkey = 'MooxNewsFrontend';
                                        }

                                        // get new file reference object to reference the added file
                                        $fileReference = $this->objectManager->get('DCNGmbH\\' . $referenceExtkey . '\Domain\Model\\' . $referenceType
                                        );
                                        $fileReference->setFile($file);
                                        $fileReference->setCruserId(0);

                                        // add new file reference to classified
                                        $item->$addMethod($fileReference);

                                        // remove temporary files in typo3 tempfolder
                                        if (file_exists($tmpFile['object']['tmp_name_local'])) {
                                            unlink($tmpFile['object']['tmp_name_local']);
                                        }
                                    }
                                }
                            }
                            // if field is the categories field
                        } elseif ($field['key'] == 'categories') {
                            // if category is selected in this form field
                            if (isset($add[$field['key']])) {
                                // if category is selected in this form field
                                if ($add[$field['key']] != '') {
                                    // prepare get method call
                                    $getMethod = 'get' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                                    // prepare add method call
                                    $addMethod = 'add' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                                    // if get and add methods are existing
                                    if (method_exists($item, $getMethod) && method_exists($item, $addMethod)) {
                                        // get selected categories from form
                                        $formItems = explode(',', $add[$field['key']]);

                                        // add new elements
                                        foreach ($formItems as $formItem) {
                                            $object = $this->categoryRepository->findByUid($formItem);
                                            if (is_object($object)) {
                                                $item->$addMethod($object);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // set fe user
                    $item->setFeCruser($this->feUser);

                    // set fe group
                    if (isset($add['fe_crgroup']) && $add['fe_crgroup'] > 0) {
                        // set variant filter
                        $feGroupFilter['uid'] = $add['fe_crgroup'];

                        // set variant filter
                        $feGroupFilter['variant'] = 'moox_community';

                        // set community admin or moderator filter
                        $feGroupFilter['communityAdminOrModerator'] = $this->feUser;

                        // check and set fe group
                        if ($this->frontendUserGroupRepository
                                ->findByFilter($feGroupFilter, $this->orderings, null, null, 'all', null)
                                ->count() > 0
                        ) {
                            $feGroup = $this->frontendUserGroupRepository->findByUid($add['fe_crgroup']);
                            if ($feGroup) {
                                $item->setFeCrgroup($feGroup);
                            }
                        }
                    }

                    // set pid (bugfix ignoring global setting);
                    if (!$item->getPid()) {
                        $item->setPid($this->settings['storagePid']);
                    }

                    // set expirations date
                    if ((int)$this->settings['expiresAfterXDays'] > 0) {
                        $expires = mktime(
                            0,
                            0,
                            0,
                            date('n'),
                            date('j') + (int)$this->settings['expiresAfterXDays'],
                            date('Y')
                        );
                        $item->setEndtime($expires);
                    }

                    // set payment
                    if ($paymentRequired) {
                        if ($paid) {
                            $item->setPaid(time());
                        }
                        $item->setPaidBy($payment['method']);
                    } else {
                        $item->setPaid(time());
                        $item->setPaidBy('user_rights');

                        /**@var \DCNGmbH\MooxCommunity\Domain\Model\AboMembership $abo*/
                        $abo = $this->feUser->getAboMembership()->current();
                        $abo->setUsedEventNote($abo->getUsedEventNote() + 1);
                        $this->aboRepository->update($abo);
                    }

                    // save item to database
                    $this->newsRepository->add($item);
                    $this->persistenceManager->persistAll();

                    // execute process datamap after database operations if available
                    //if (class_exists('Tx_MooxNews_Hooks_Tcemain')) {
                        //$Tcemain = $this->objectManager->get('Tx_MooxNews_Hooks_Tcemain');
                    $tceMain = $this->objectManager->get(Tcemain::class);
                    if (is_object($tceMain)) {
                        $tceMain->processDatamap_afterDatabaseOperations(
                            null,
                            'tx_mooxnews_domain_model_news',
                            (string)$item->getUid(),
                            array(),
                            $this->objectManager->get(DataHandler::class)
                        );
                    }
                    //}

                    // add message
                    $messages[] = array(
                        'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                        'title' => LocalizationUtility::translate(
                            self::LLPATH . 'pi1.action_add',
                            $this->extensionName
                        ),
                        'text' => LocalizationUtility::translate(
                            self::LLPATH . 'pi1.action_add.success',
                            $this->extensionName
                        ),
                        'type' => FlashMessage::OK
                    );

                    // set flash messages
                    $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);

                    // redirect to list view/payment
                    if ($returnUrl) {
                        if ($paymentRequired && !$paid) {
                            $redirectUri = $this->uriBuilder->setNoCache(1)->uriFor(
                                'payment',
                                array('item' => $item->getUid(), 'payment' => $payment, 'returnUrl' => $returnUrl),
                                'Pi1',
                                'MooxNewsFrontend',
                                'Pi1'
                            );
                            $this->redirectToUri($redirectUri);
                        } else {
                            $this->redirectToUri($returnUrl);
                        }
                    } else {
                        if ($paymentRequired && !$paid) {
                            $redirectUri = $this->uriBuilder->setNoCache(1)->uriFor(
                                'payment',
                                array('item' => $item->getUid(), 'payment' => $payment),
                                'Pi1',
                                'MooxNewsFrontend',
                                'Pi1'
                            );
                            $this->redirectToUri($redirectUri);
                        } else {
                            $this->redirect('list');
                        }
                    }
                    unset($add);
                }
            }
        } elseif (!$reload) {
            // go trough all registered fields and set default values
            foreach ($fields as $field) {
                // if field is a file field
                if ($field['config']['default'] != '') {
                    // preset form field
                    $add[$field['key']] = $field['config']['default'];
                }
            }
        }

        // set default payment method
        if ($paymentRequired && !isset($payment['method'])) {
            $payment['method'] = current($paymentMethods)['extkey'];
        }

        // set default type
        if (!isset($add['type'])) {
            $add['type'] = $type;
        }


        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
        $assignMultiple = [
            'add' => $add,
            'fields' => $fields,
            'errors' => $errors,
            'partialRootPath' => $this->partialRootPath,
            'paymentMethods' => $paymentMethods,
            'payment' => $payment,
            'returnUrl' => $returnUrl,
            'action' => 'add'
        ];
        $this->view->assignMultiple($assignMultiple);
    }

    /**
     * action edit
     *
     * @param int $item
     * @param array $edit
     * @param string $returnUrl
     * @param bool $reload
     * @return void
     */
    public function editAction($item = null, $edit = null, $returnUrl = null, $reload = false)
    {
        $messages = array();
        $errors = array();
        $fileAddOnly = false;
        $editable = false;

        if ($item > 0) {
            $item = $this->newsRepository->findByUid($item, false);
        }

        // if user is logged in and correct item is passed
        if ($this->feUser && is_a($item, '\DCNGmbH\MooxNewsFrontend\Domain\Model\News')) {
            if ($this->feUser->getUid() == $item->getFeCruser()->getUid()) {
                $editable = true;
            }
        }

        // if item is editable by current user
        if (!$editable) {
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_edit', $this->extensionName),
                'text' => LocalizationUtility::translate(
                    self::LLPATH . 'pi1.action_edit.error.no_access',
                    $this->extensionName
                ),
                'type' => FlashMessage::ERROR
            );

            $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
            $this->redirect('error');
        }
        
        // set type
        if (isset($edit['type']) && $edit['type'] != '') {
            $type = $edit['type'];
        } elseif ($item->getType() != '') {
            $type = $item->getType();
        } elseif ($this->type) {
            $type = $this->type;
        } else {
            $type = GeneralUtility::camelCaseToLowerCaseUnderscored($this->allowedTypes[0]);
        }

        // get fields set by plugin
        $fields = $this->helperService->configureFields(
            $this->settings['editFields'],
            $this->settings['requiredEditFields'],
            $this->settings,
            $this->fieldConfig,
            $type,
            $this->allowedTypes
        );

        // process form values
        if ($edit && !$reload) {
            // add or remove files by add button
            foreach ($fields as $field) {
                // if field is a file field
                if ($field['config']['type'] == 'file') {
                    // if temporary file form fields are set
                    if (is_array($edit[$field['key'] . '_tmp'])) {
                        // go through all temporary file form fields
                        foreach ($edit[$field['key'] . '_tmp'] as $key => $value) {
                            // if remove button for this temporary field is set
                            if (isset($edit[$field['key'] . '_remove_' . $key])) {
                                // prepare get method call
                                $getMethod = 'get' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                                // prepare remove method call
                                $removeMethod = 'remove' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                                // if get and remove methods are existing
                                if (method_exists($item, $getMethod) && method_exists($item, $removeMethod)) {
                                    // go through all current files
                                    foreach ($item->$getMethod() as $reference) {
                                        // temporary field key equals reference key from object storage
                                        if ($reference->getUid() == $key) {
                                            // remove selected file
                                            $item->$removeMethod($reference);

                                            // save changes to database
                                            $this->newsRepository->update($item);
                                            $this->persistenceManager->persistAll();
                                        }
                                    }
                                }

                                $messages[] = array(
                                    'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                                    'title' => LocalizationUtility::translate(
                                        self::LLPATH . 'pi1.action_edit',
                                        $this->extensionName
                                    ),
                                    'text' => LocalizationUtility::translate(
                                        self::LLPATH . 'pi1.action_all.removed.' . $field['config']['reference-type'],
                                        $this->extensionName
                                    ),
                                    'type' => FlashMessage::OK
                                );
                                $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
                                $this->redirect('edit', null, null, array('item' => $item));
                            }
                        }
                    }

                    // if form file add button was pressed for this field
                    if (isset($edit[$field['key']]) && isset($edit[$field['key'] . '_add'])) {
                        // if file is selected in this form file field
                        if ($edit[$field['key']]['name'] != '') {
                            // prepare get method call
                            $getMethod = 'get' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                            // prepare add method call
                            $addMethod = 'add' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                            // if get and add methods are existing
                            if (method_exists($item, $getMethod) && method_exists($item, $addMethod)) {
                                // get current count of temporary file form fields for this file field
                                $fileCnt = $item->$getMethod()->count();

                                // if maxitems not reached yet for this field
                                if (!isset($field['config']['maxitems'])
                                    || (isset($field['config']['maxitems']) && $fileCnt < $field['config']['maxitems'])
                                ) {
                                    // get file storage
                                    $storage = $this->storageRepository->findByUid($this->fileStorageUid);

                                    // create file storage folder if not already existing
                                    $folder = $storage->createFolder($this->fileStorageFolder);

                                    // add file to storage
                                    $fileObject = $storage->addFile(
                                        $edit[$field['key']]['tmp_name'],
                                        $folder, $edit[$field['key']]['name']
                                    );

                                    // get recently added file object from storage
                                    $fileObject = $storage->getFile($fileObject->getIdentifier());

                                    // get real file object from file repo
                                    $file = $this->fileRepository->findByUid($fileObject->getProperty('uid'));

                                    // set reference type depending on field
                                    if ($field['config']['reference-type'] == 'image') {
                                        $referenceType = 'ImageReference';
                                    } else {
                                        $referenceType = 'FileReference';
                                    }

                                    // set reference extkey
                                    if ($field['config']['extkeyUCC']
                                        && $field['config']['extkeyUCC'] != 'MooxNewsFrontend'
                                        && $field['config']['extkeyUCC'] != 'MooxNews'
                                    ) {
                                        $referenceExtkey = $field['config']['extkeyUCC'];
                                    } else {
                                        $referenceExtkey = 'MooxNewsFrontend';
                                    }

                                    // get new file reference object to reference the added file
                                    $fileReference = $this->objectManager->get(
                                        'DCNGmbH\\' . $referenceExtkey . '\Domain\Model\\' . $referenceType
                                    );
                                    $fileReference->setFile($file);
                                    $fileReference->setCruserId(0);

                                    // add new file reference to classified
                                    $item->$addMethod($fileReference);

                                    // save changes to database
                                    $this->newsRepository->update($item);
                                    $this->persistenceManager->persistAll();

                                    $messages[] = array(
                                        'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                                        'title' => LocalizationUtility::translate(
                                            self::LLPATH . 'pi1.action_edit',
                                            $this->extensionName
                                        ),
                                        'text' => LocalizationUtility::translate(
                                            self::LLPATH . 'pi1.action_all.added.' . $field['config']['reference-type'],
                                            $this->extensionName
                                        ),
                                        'type' => FlashMessage::OK
                                    );
                                    $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);

                                    // redirect to error view
                                    $this->redirect(
                                        'edit',
                                        null,
                                        null,
                                        array('item' => $item, 'returnUrl' => $returnUrl)
                                    );
                                } else {
                                    $message = LocalizationUtility::translate(
                                        self::LLPATH . 'form.' . $field['key'] . '.error.too_many',
                                        $this->extensionName,
                                        array($field['config']['maxitems'])
                                    );
                                    if ($message == '') {
                                        $message = LocalizationUtility::translate(
                                            str_replace('moox_news_frontend', $field['extkey'], self::LLPATH)
                                            . 'form.' . $field['key'] . '.error.too_many',
                                            $field['extkey'],
                                            array($field['config']['maxitems'])
                                        );
                                    }
                                    if ($message == '') {
                                        $message = LocalizationUtility::translate(
                                            self::LLPATH . 'form.error.too_many',
                                            $this->extensionName,
                                            array($field['config']['maxitems'])
                                        );
                                    }
                                    $title = LocalizationUtility::translate(
                                        self::LLPATH . 'form.' . $field['key'],
                                        $this->extensionName
                                    );
                                    if ($title == '') {
                                        $title = LocalizationUtility::translate(
                                            str_replace('moox_news_frontend', $field['extkey'], self::LLPATH)
                                            . 'form.' . $field['key'],
                                            $field['extkey']
                                        );
                                    }
                                    $messages[] = array(
                                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                                        'title' => $title,
                                        'text' => $message,
                                        'type' => FlashMessage::ERROR
                                    );

                                    // set error
                                    $errors[$field['key']] = true;
                                }
                            }
                        } else {
                            $message = LocalizationUtility::translate(
                                self::LLPATH . 'form.' . $field['key'] . '.error.empty',
                                $this->extensionName
                            );
                            if ($message == '') {
                                $message = LocalizationUtility::translate(
                                    str_replace('moox_news_frontend', $field['extkey'], self::LLPATH)
                                    . 'form.' . $field['key'] . '.error.empty',
                                    $field['extkey']
                                );
                            }
                            if ($message == '') {
                                $message = LocalizationUtility::translate(
                                    self::LLPATH . 'form.error.empty',
                                    $this->extensionName
                                );
                            }
                            $title = LocalizationUtility::translate(
                                self::LLPATH . 'form.' . $field['key'],
                                $this->extensionName
                            );
                            if ($title == '') {
                                $title = LocalizationUtility::translate(
                                    str_replace('moox_news_frontend', $field['extkey'], self::LLPATH)
                                    . 'form.' . $field['key'],
                                    $field['extkey']
                                );
                            }
                            $messages[] = array(
                                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                                'title' => $title,
                                'text' => $message,
                                'type' => FlashMessage::ERROR
                            );

                            // set error
                            $errors[$field['key']] = true;
                        }

                        // set "file add only" to prevent further processing of form values
                        $fileAddOnly = true;
                    }
                }
            }

            // if general save button is pressed
            if (!$fileAddOnly) {
                // check fields
                $this->helperService->checkFields($fields, $edit, $messages, $errors);

                // set item values
                foreach ($fields as $field) {
                    if (!$field['passthrough'] && !in_array($field['config']['type'], array('file', 'tree'))
                        && !in_array($field['key'], array('type', 'categories', 'fe_crgroup'))
                    ) {
                        $setMethod = 'set' . GeneralUtility::underscoredToUpperCamelCase($field['key']);
                        if (method_exists($item, $setMethod) && isset($edit[$field['key']])) {
                            if (in_array($field['config']['type'], array('date'))) {
                                $item->$setMethod($this->helperService->getTimestamp($edit[$field['key']]));
                            } elseif (in_array($field['config']['type'], array('datetime'))) {
                                $item->$setMethod(
                                    $this->helperService->getTimestamp(
                                        $edit[$field['key']],
                                        $edit[$field['key'] . '_hh'],
                                        $edit[$field['key'] . '_mm']
                                    )
                                );
                            } else {
                                $item->$setMethod((is_array($edit[$field['key']])) ? implode(',', $edit[$field['key']]) : trim($edit[$field['key']]));
                            }
                        }
                    }
                }

                // no errors -> save changes
                if (!count($errors)) {
                    // set item type
                    $item->setType($type);

                    // set file relations
                    foreach ($fields as $field) {
                        // if field is a file field
                        if (in_array($field['config']['type'], array('file'))) {
                            // if file is selected in this form file field
                            if (isset($edit[$field['key']])) {
                                // if file is selected in this form file field
                                if ($edit[$field['key']]['name'] != '') {
                                    // prepare get method call
                                    $getMethod = 'get' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                                    // prepare add method call
                                    $addMethod = 'add' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                                    // if get and add methods are existing
                                    if (method_exists($item, $getMethod) && method_exists($item, $addMethod)) {
                                        // get file storage
                                        $storage = $this->storageRepository->findByUid($this->fileStorageUid);

                                        // create file storage folder if not already existing
                                        $folder = $storage->createFolder($this->fileStorageFolder);

                                        // add file to storage
                                        $fileObject = $storage->addFile(
                                            $edit[$field['key']]['tmp_name'],
                                            $folder,
                                            $edit[$field['key']]['name']
                                        );

                                        // get recently added file object from storage
                                        $fileObject = $storage->getFile($fileObject->getIdentifier());

                                        // get real file object from file repo
                                        $file = $this->fileRepository->findByUid($fileObject->getProperty('uid'));

                                        // set reference type depending on field
                                        if ($field['config']['reference-type'] == 'image') {
                                            $referenceType = 'ImageReference';
                                        } else {
                                            $referenceType = 'FileReference';
                                        }

                                        // set reference extkey
                                        if ($field['config']['extkeyUCC']
                                            && $field['config']['extkeyUCC'] != 'MooxNewsFrontend'
                                            && $field['config']['extkeyUCC'] != 'MooxNews'
                                        ) {
                                            $referenceExtkey = $field['config']['extkeyUCC'];
                                        } else {
                                            $referenceExtkey = 'MooxNewsFrontend';
                                        }

                                        // get new file reference object to reference the added file
                                        $fileReference = $this->objectManager->get(
                                            'DCNGmbH\\' . $referenceExtkey . '\Domain\Model\\' . $referenceType
                                        );
                                        $fileReference->setFile($file);
                                        $fileReference->setCruserId(0);

                                        // add new file reference to classified
                                        $item->$addMethod($fileReference);
                                    }
                                }
                            }
                            // if field is the categories field
                        } elseif ($field['key'] == 'categories') {
                            // if file is selected in this form file field
                            if (isset($edit[$field['key']])) {
                                // if file is selected in this form file field
                                if ($edit[$field['key']] != '') {
                                    // prepare get method call
                                    $getMethod = 'get' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                                    // prepare set method call
                                    $setMethod = 'set' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                                    // prepare add method call
                                    $addMethod = 'add' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                                    // prepare remove method call
                                    $removeMethod = 'remove' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                                    // if get and add methods are existing
                                    if (method_exists($item, $getMethod)
                                        && method_exists($item, $setMethod)
                                        && method_exists($item, $addMethod)
                                        && method_exists($item, $removeMethod)
                                    ) {
                                        // get selected categories from form
                                        $formItems = explode(',', $edit[$field['key']]);

                                        // reset object storage
                                        $item->$setMethod(new \TYPO3\CMS\Extbase\Persistence\ObjectStorage());

                                        // add new elements
                                        foreach ($formItems as $formItem) {
                                            $object = $this->categoryRepository->findByUid($formItem);
                                            if (is_object($object)) {
                                                $item->$addMethod($object);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // set fe group
                    if (isset($edit['fe_crgroup']) && $edit['fe_crgroup'] > 0) {
                        // set variant filter
                        $feGroupFilter['uid'] = $edit['fe_crgroup'];

                        // set variant filter
                        $feGroupFilter['variant'] = 'moox_community';

                        // set community admin or moderator filter
                        $feGroupFilter['communityAdminOrModerator'] = $this->feUser;

                        // check and set fe group
                        if ($this->frontendUserGroupRepository
                                ->findByFilter(
                                    $feGroupFilter,
                                    $this->orderings,
                                    null,
                                    null,
                                    'all',
                                    null
                                )
                                ->count() > 0
                        ) {
                            $feGroup = $this->frontendUserGroupRepository->findByUid($edit['fe_crgroup']);
                            if ($feGroup) {
                                $item->setFeCrgroup($feGroup);
                            }
                        }
                    } elseif (isset($edit['fe_crgroup'])) {
                        $item->setFeCrgroup(null);
                    }

                    // save item to database
                    $this->newsRepository->update($item);
                    $this->persistenceManager->persistAll();

                    // execute process datamap after database operations if available
                    if (class_exists('Tx_MooxNews_Hooks_Tcemain')) {
                        $Tcemain = $this->objectManager->get('Tx_MooxNews_Hooks_Tcemain');
                        if (is_object($Tcemain)) {
                            $Tcemain->processDatamap_afterDatabaseOperations(
                                null,
                                'tx_mooxnews_domain_model_news',
                                (string)$item->getUid(),
                                array(),
                                null
                            );
                        }
                    }

                    $messages[] = array(
                        'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                        'title' => LocalizationUtility::translate(
                            self::LLPATH . 'pi1.action_edit',
                            $this->extensionName
                        ),
                        'text' => LocalizationUtility::translate(
                            self::LLPATH . 'pi1.action_edit.success',
                            $this->extensionName
                        ),
                        'type' => FlashMessage::OK
                    );
                    $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
                    $this->redirect('edit', null, null, array('item' => $item, 'returnUrl' => $returnUrl));
                }
            }
        }

        // preset edit files if existing
        if (!$edit && !$reload) {
            // go trough all registered fields
            foreach ($fields as $field) {
                // prepare get method call
                $getMethod = 'get' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                // if get method exists
                if (method_exists($item, $getMethod)) {
                    // if field is a file field
                    if (in_array($field['config']['type'], array('file'))) {
                        // go through all existing files of this field
                        foreach ($item->$getMethod() as $fileReference) {
                            // get file reference
                            $fileReference = $fileReference->getOriginalResource();

                            // get real file
                            $file = $fileReference->getOriginalFile();

                            // get current count of temporary file form fields for this file field
                            $fileCnt = (is_array($edit[$field['key'] . '_tmp'])) ? count($edit[$field['key'] . '_tmp']) : 0;

                            // set temporary file form fields for existing file
                            $edit[$field['key'] . '_tmp'][$fileCnt]['name'] = $file->getName();
                            $edit[$field['key'] . '_tmp'][$fileCnt]['src'] = $file->getPublicUrl();
                            $edit[$field['key'] . '_tmp'][$fileCnt]['ext'] = $fileReference->getExtension();
                            $edit[$field['key'] . '_tmp'][$fileCnt]['size'] = $fileReference->getSize();
                            $edit[$field['key'] . '_tmp'][$fileCnt]['reference'] = $fileReference;
                        }
                        // if field is a tree field
                    } elseif (in_array($field['config']['type'], array('tree'))) {
                        // init tree
                        $treeItems = array();

                        // go through all existing tree items
                        foreach ($item->$getMethod() as $treeItem) {
                            // add to tree
                            $treeItems[] = $treeItem->getUid();
                        }

                        // set temporary file form field for existing tree items
                        $edit[$field['key']] = implode(',', $treeItems);
                    } elseif (in_array($field['config']['type'], array('check'))) {
                        $value = ($item->$getMethod() != '') ? explode(',', $item->$getMethod()) : '';
                        if (!is_array($field['config']['items'])
                            || (is_array($field['config']['items']) && count($field['config']['items']) < 2)
                        ) {
                            $value = $value[0];
                        }
                        $edit[$field['key']] = $value;
                    } elseif (in_array($field['config']['type'], array('date')) && $item->$getMethod() != '') {
                        if (is_object($item->$getMethod()) && get_class($item->$getMethod()) == 'DateTime') {
                            $value = $item->$getMethod()->getTimestamp();
                        } else {
                            $value = $item->$getMethod();
                        }

                        $edit[$field['key']] = date('d.m.Y', $value);
                    } elseif (in_array($field['config']['type'], array('datetime')) && $item->$getMethod() != '') {
                        if (is_object($item->$getMethod()) && get_class($item->$getMethod()) == 'DateTime') {
                            $value = $item->$getMethod()->getTimestamp();
                        } else {
                            $value = $item->$getMethod();
                        }

                        $edit[$field['key']] = date('d.m.Y', $value);
                        $edit[$field['key'] . '_hh'] = date('H', $value);
                        $edit[$field['key'] . '_mm'] = date('i', $value);
                    } elseif ($field['key'] == 'fe_crgroup') {
                        $feGroup = $item->$getMethod();

                        if (is_object($feGroup)) {
                            $edit[$field['key']] = $feGroup->getUid();
                        }
                    } elseif (!$field['passthrough']) {
                        // preset form field
                        $edit[$field['key']] = $item->$getMethod();
                    }
                }
            }
        }

        // set current type
        if (!isset($edit['type'])) {
            $edit['type'] = $type;
        }

        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);

        // set template variables
        $this->view->assign('item', $item);
        $this->view->assign('edit', $edit);
        $this->view->assign('fields', $fields);
        $this->view->assign('errors', $errors);
        $this->view->assign('returnUrl', $returnUrl);
        $this->view->assign('action', 'edit');
    }

    /**
     * action payment
     *
     * @param int $item
     * @param array $payment
     * @param string $returnUrl
     * @param bool $process
     * @param bool $check
     * @return void
     */
    public function paymentAction($item = null, $payment = null, $returnUrl = null, $process = false, $check = false)
    {
        $messages = [];
        $errors = [];

        if ($item > 0) {
            // get payment methods
            if (is_object($this->paymentService)) {
                $paymentMethods = $this->paymentService->getPaymentMethods($this->settings['paymentMethods']);
            }

            if ($item > 0) {
                $item = $this->newsRepository->findByUid($item, false);
            }

            // get payment config
            $config = $this->helperService->getPaymentConfig(
                (isset($payment['method']) && $payment['method'] != '') ? $payment['method'] : $item->getPaidBy()
            );

            if (!is_null($payment)) {
                if (count($config) && count($payment)) {
                    $payment['amount'] = $this->settings['paymentAmount'];

                    if ($check) {
                        if ($this->helperService->checkPayment($payment, $messages, $errors)) {
                            $item->setPaid(time());
                            $item->setPaidBy($payment['method']);
                            $item->setPaidInfo('');

                            // save changes to database
                            $this->newsRepository->update($item);
                            $this->persistenceManager->persistAll();

                            $messages[] = array(
                                'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                                'title' => LocalizationUtility::translate(
                                    self::LLPATH . 'pi1.action_payment',
                                    $this->extensionName
                                ),
                                'text' => LocalizationUtility::translate(
                                    self::LLPATH . 'pi1.action_payment.success',
                                    $this->extensionName
                                ),
                                'type' => FlashMessage::OK
                            );
                            $this->helperService->setFlashMessages($this, $messages);

                            // redirect to list view/returnUrl
                            if ($returnUrl) {
                                $this->redirectToUri($returnUrl);
                            } else {
                                $this->redirect('list');
                            }
                        }
                    }

                    // no errors -> add classified
                    if (!count($errors)) {
                        if ($process) {
                            $processed = $this->helperService->processPayment($payment, $messages, $errors);

                            if ($processed['paid']) {
                                $item->setPaid(time());
                                $item->setPaidInfo(json_encode($processed['response']));
                                $this->newsRepository->update($item);
                                $this->persistenceManager->persistAll();

                                $messages[] = array(
                                    'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                                    'title' => LocalizationUtility::translate(
                                        self::LLPATH . 'pi1.action_payment',
                                        $this->extensionName
                                    ),
                                    'text' => LocalizationUtility::translate(
                                        self::LLPATH . 'pi1.action_payment.success',
                                        $this->extensionName
                                    ),
                                    'type' => FlashMessage::OK
                                );
                            } else {
                                $item->setPaidInfo(json_encode($processed['response']));
                                $this->newsRepository->update($item);
                                $this->persistenceManager->persistAll();

                                $messages[] = array(
                                    'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                                    'title' => LocalizationUtility::translate(
                                        self::LLPATH . 'pi1.action_payment',
                                        $this->extensionName
                                    ),
                                    'text' => LocalizationUtility::translate(
                                        self::LLPATH . 'pi1.action_payment.error',
                                        $this->extensionName
                                    ),
                                    'type' => FlashMessage::ERROR
                                );

                                $this->helperService->setFlashMessages($this, $messages);
                                $redirectUri = $this->uriBuilder
                                    ->setNoCache(1)
                                    ->uriFor(
                                        'payment',
                                        array('item' => $item->getUid(), 'payment' => $payment, 'returnUrl' => $returnUrl),
                                        'Pi1',
                                        'MooxNewsFrontend',
                                        'Pi1'
                                    );
                                $this->redirectToUri($redirectUri);
                            }
                            $this->helperService->setFlashMessages($this, $messages);

                            // redirect to list view/returnUrl
                            if ($returnUrl) {
                                $this->redirectToUri($returnUrl);
                            } else {
                                $this->redirect('list');
                            }

                            $partial = 'Process';
                        } else {
                            $prepared = $this->helperService->preparePayment($payment, $messages, $errors);

                            // connection lost?
                            if (is_array($prepared)) {
                                $prepared['processUrl'] = $this->uriBuilder
                                    ->setNoCache(true)
                                    ->setCreateAbsoluteUri(true)
                                    ->uriFor(
                                        'payment',
                                        array('item' => $item->getUid(), 'payment' => $payment, 'returnUrl' => $returnUrl, 'process' => 1),
                                        'Pi1',
                                        'MooxNewsFrontend',
                                        'Pi1'
                                    );
                                $partial = 'Prepare';
                            } else {
                                $this->redirect('error');
                            }
                        }
                    }
                }
            } else {
                if ($item->getPaid() > 0) {
                    $partial = 'Info';
                } else {
                    // set default payment method
                    if (!isset($payment['method'])) {
                        $payment['method'] = current($paymentMethods)['extkey'];
                    }

                    $payment['item'] = $item->getUid();
                }
            }

            // append payment config
            $payment['config'] = $config;
            if ($item->getPaidInfo() != '') {
                $payment['info'] = json_decode($item->getPaidInfo(), true);
            }

            $this->helperService->setFlashMessages($this, $messages);
            $multipleAssign = array(
                'item' => $item,
                'payment' => $payment,
                'paymentMethods' => $paymentMethods,
                'prepared' => $prepared,
                'processed' => $processed,
                'partial' => $partial,
                'returnUrl' => $returnUrl,
                'action' => 'payment'
            );
            $this->view->assignMultiple($multipleAssign);
        }
    }

    /**
     * action delete
     *
     * @param int $item
     * @param string $returnUrl
     * @return void
     */
    public function deleteAction($item = null, $returnUrl = null)
    {
        $messages = array();
        $deleteAble = false;

        if ($item > 0) {
            $item = $this->newsRepository->findByUid($item, false);
        }

        // if user is logged in and correct item is passed
        if ($this->feUser && is_a($item, '\DCNGmbH\MooxNewsFrontend\Domain\Model\News')) {
            if ($this->feUser->getUid() == $item->getFeCruser()->getUid()) {
                $deleteAble = true;
            }
        }

        // if item is editable by current user
        if (!$deleteAble) {
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_delete', $this->extensionName),
                'text' => LocalizationUtility::translate(
                    self::LLPATH . 'pi1.action_delete.error.no_access',
                    $this->extensionName
                ),
                'type' => FlashMessage::ERROR
            );
            $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
            $this->redirect('error');
        }

        // remove item from database
        $this->newsRepository->remove($item);
        $this->persistenceManager->persistAll();

        $messages[] = array(
            'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
            'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_delete', $this->extensionName),
            'text' => LocalizationUtility::translate(
                self::LLPATH . 'pi1.action_delete.success',
                $this->extensionName
            ),
            'type' => FlashMessage::OK
        );
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);

        // redirect to list view
        if ($returnUrl) {
            $this->redirectToUri($returnUrl);
        } else {
            $this->redirect('list');
        }

        // set template variables
        $this->view->assign('item', $item);
        $this->view->assign('action', 'delete');
    }

    /**
     * action visibility
     *
     * @param int $item
     * @param string $returnUrl
     * @return void
     */
    public function visibilityAction($item = null, $returnUrl = null)
    {
        $messages = array();
        $editable = false;

        if ($item > 0) {
            $item = $this->newsRepository->findByUid($item, false);
        }

        // if user is logged in and correct item is passed
        if ($this->feUser && is_a($item, '\DCNGmbH\MooxNewsFrontend\Domain\Model\News')) {
            if ($this->feUser->getUid() == $item->getFeCruser()->getUid()) {
                $editable = true;
            }
        }

        // if item is editable by current user
        if (!$editable) {
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_visibility', $this->extensionName),
                'text' => LocalizationUtility::translate(
                    self::LLPATH . 'pi1.action_visibility.error.no_access',
                    $this->extensionName
                ),
                'type' => FlashMessage::ERROR
            );
            $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
            $this->redirect('error');
        }

        // change visibility
        if ($item->getHidden() == 1) {
            $item->setHidden(0);
        } else {
            $item->setHidden(1);
        }

        // remove item from database
        $this->newsRepository->update($item);
        $this->persistenceManager->persistAll();

        $messages[] = array(
            'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
            'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_visibility', $this->extensionName),
            'text' => LocalizationUtility::translate(
                self::LLPATH . 'pi1.action_visibility.success',
                $this->extensionName
            ),
            'type' => FlashMessage::OK
        );
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);

        // redirect to list view
        if ($returnUrl) {
            $this->redirectToUri($returnUrl);
        } else {
            $this->redirect('list');
        }

        // set template variables
        $this->view->assign('item', $item);
        $this->view->assign('action', 'visibility');
    }

    /**
     * action privacy
     *
     * @param int $item
     * @param string $returnUrl
     * @return void
     */
    public function privacyAction($item = null, $returnUrl = null)
    {
        $messages = array();
        $editable = false;

        if ($item > 0) {
            $item = $this->newsRepository->findByUid($item, false);
        }

        // if user is logged in and correct item is passed
        if ($this->feUser && is_a($item, '\DCNGmbH\MooxNewsFrontend\Domain\Model\News')) {
            if ($this->feUser->getUid() == $item->getFeCruser()->getUid()) {
                $editable = true;
            }
        }

        if (!$editable) {
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_privacy', $this->extensionName),
                'text' => LocalizationUtility::translate(
                    self::LLPATH . 'pi1.action_privacy.error.no_access',
                    $this->extensionName
                ),
                'type' => FlashMessage::ERROR
            );

            $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
            $this->redirect('error');
        }

        // change visibility
        if ($item->getPrivacy() == 1) {
            $item->setPrivacy(0);
        } else {
            $item->setPrivacy(1);
        }

        // remove item from database
        $this->newsRepository->update($item);
        $this->persistenceManager->persistAll();

        // add message
        $messages[] = array(
            'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
            'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_privacy', $this->extensionName),
            'text' => LocalizationUtility::translate(
                self::LLPATH . 'pi1.action_privacy.success',
                $this->extensionName
            ),
            'type' => FlashMessage::OK
        );

        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);

        // redirect to list view
        if ($returnUrl) {
            $this->redirectToUri($returnUrl);
        } else {
            $this->redirect('list');
        }

        // set template variables
        $this->view->assign('item', $item);
        $this->view->assign('action', 'privacy');
    }

    /**
     * action error
     *
     * @return void
     */
    public function errorAction()
    {
        $this->view->assign('action', 'error');
    }

    /**
     * Returns fields
     *
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Set fields
     *
     * @param array $fields fields
     * @return void
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
    }

    /**
     * Returns fieldConfig
     *
     * @return array
     */
    public function getFieldConfig()
    {
        return $this->fieldConfig;
    }

    /**
     * Set fieldConfig
     *
     * @param array $fieldConfig fieldConfig
     * @return void
     */
    public function setFieldConfig($fieldConfig)
    {
        $this->fieldConfig = $fieldConfig;
    }

    /**
     * Returns ext conf
     *
     * @return array
     */
    public function getExtConf()
    {
        return $this->extConf;
    }

    /**
     * Set ext conf
     *
     * @param array $extConf ext conf
     * @return void
     */
    public function setExtConf($extConf)
    {
        $this->extConf = $extConf;
    }
}
