<?php
$extensionClassesPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('moox_news_frontend') . 'Classes/';
require_once($extensionClassesPath.'Cache/ClassCacheBuilder.php');

$default = array(
	'DCNGmbH\MooxNewsFrontend\Cache\ClassCacheBuilder' => $extensionClassesPath . 'Cache/ClassCacheBuilder.php',
	'DCNGmbH\MooxNewsFrontend\Domain\Model\FrontendNews' => $extensionClassesPath . 'Domain/Model/FrontendNews.php',
);

/** @var DCNGmbH\MooxNewsFrontend\Cache\ClassCacheBuilder $classCacheBuilder */
$classCacheBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('DCNGmbH\MooxNewsFrontend\Cache\ClassCacheBuilder');
$mergedClasses = array_merge($default, $classCacheBuilder->build());
return $mergedClasses;
?>
