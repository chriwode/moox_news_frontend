<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

// register frontend plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'DCNGmbH.' . $_EXTKEY,
    'Pi1',
    array(
        'Pi1' => 'list,detail,add,edit,delete,visibility,privacy,error,payment',
    ),
    // non-cacheable actions
    array(
        'Pi1' => 'list,detail,add,edit,delete,visibility,privacy,error,payment',
    )
);

// register frontend plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'DCNGmbH.' . $_EXTKEY,
    'Pi2',
    array(
        'Pi2' => 'list,detail,error',
    ),
    // non-cacheable actions
    array(
        'Pi2' => 'list,detail,error',
    )
);
