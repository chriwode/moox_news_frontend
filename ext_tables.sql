#
# Extend table structure of table 'tx_mooxnews_domain_model_news'
#
CREATE TABLE tx_mooxnews_domain_model_news (
	fe_cruser int(11) unsigned DEFAULT '0' NOT NULL,
	fe_crgroup int(11) unsigned DEFAULT '0' NOT NULL,
	paid int(11) unsigned DEFAULT '0' NOT NULL,
	paid_by tinyint(4) unsigned DEFAULT '0' NOT NULL,
	paid_info text NOT NULL,
	privacy tinyint(4) unsigned DEFAULT '0' NOT NULL,
);