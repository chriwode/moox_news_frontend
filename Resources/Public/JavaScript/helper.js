function mooxNewsFrontendInitClearButtons(){
	$('.tx-moox-news-frontend .has-clear input, .has-clear textarea').on('keyup', function() {
        if ($(this).val() == '') {
            $(this).parents('.form-group').addClass('has-empty-value');
        } else {
            $(this).parents('.form-group').removeClass('has-empty-value');
        }		
    }).trigger('change');

    $('.tx-moox-news-frontend .has-clear .form-control-clear').on('click', function() {
        var input = $(this).parents('.form-group').find('input,textarea');
		input.val('').trigger('change');
		input.val('').trigger('keyup');

        // Trigger a "cleared" event on the input for extensibility purpose
        input.trigger('cleared');
    });	
	
	$('.tx-moox-news-frontend .has-clear input, .has-clear textarea').each(function() {
        if ($(this).val() == '') {
            $(this).parents('.form-group').addClass('has-empty-value');
        } else {
            $(this).parents('.form-group').removeClass('has-empty-value');
        }		
    });
}
function mooxNewsFrontendInitComplexify(){
	$('.tx-moox-news-frontend input.complexify').complexify({}, function (valid, complexity) {
		var progressBar = $('#complexity-bar');
		progressBar.toggleClass('progress-bar-success', valid);
		progressBar.toggleClass('progress-bar-danger', !valid);
		progressBar.css({'width': complexity + '%'});
	});
}

function mooxNewsFrontendInitDatePicker(){
	$('.tx-moox-news-frontend .input-group.date').datepicker({
		format: 'dd.mm.yyyy',
		todayBtn: true,
		clearBtn: true,		
		language: 'de',
		calendarWeeks: true,
		autoclose: true,
		todayHighlight: true
	});
	$('.tx-moox-news-frontend .input-group.date.time').datepicker({
		format: 'hh:ss dd.mm.yyyy',
		todayBtn: true,
		clearBtn: true,		
		language: 'de',
		calendarWeeks: true,
		autoclose: true,
		todayHighlight: true
	});
}
function mooxNewsFrontendInitTooltips(){
	$("body .tx-moox-news-frontend ").tooltip({ selector: '[data-toggle=tooltip]' });
}
function mooxNewsFrontendInitDropdowns(){
	$('.tx-moox-news-frontend .dropdown-toggle').dropdown()
}
function mooxNewsFrontendInitTreeSelector(){
	$(".tx-moox-news-frontend .tree-selector").bind("loaded.jstree", function(event, data) { 
		data.instance.open_all();
	});
		
	$(".tx-moox-news-frontend .tree-selector.tree-selector-multiple").jstree({		
		"core" : {
			"themes" : {
				"type" : "large",
				//"icons" : false,
			},
		},
		"plugins" : [ "wholerow", "types", "checkbox" ],
		"types" : {
			"default" : {
				"icon" : "glyphicon glyphicon-tag"
			},
		},
		"checkbox" : {
			"three_state" : false,					
		},
	});
	
	$(".tx-moox-news-frontend .tree-selector.tree-selector-single").jstree({
		"core" : {
			"themes" : {
				"type" : "large",				
			},
		},
		"plugins" : [ "wholerow", "types" ],
		"types" : {
			"default" : {
				"icon" : "glyphicon glyphicon-tag"
			},
		},
	});

	$(".tx-moox-news-frontend .tree-selector-wrapper").each(function(){
		
		hidden = $(this).children('input.tree-selector-selected').first();
		var tree = $(this).children('.tree-selector').first();
		
		if(hidden.length>0){
			if(hidden.val()!=""){
				selected = hidden.val().split(",");
				selected.forEach(function(uid) {
					tree.jstree('select_node', 'tree-item-' + uid);					
				});
			}
		}
		
	});
	
	$(".tx-moox-news-frontend .tree-selector").on("changed.jstree", function (e, data) {
		
		id = $(this).attr('id');
		hidden = $(this).parents('div.tree-selector-wrapper').first().children('input.tree-selector-selected').first();
		var tree = $(this);
		
		var categories = "";
		if(tree.hasClass("tree-selector-cascade")){
			$('#'+id+' li[aria-selected=true]').each(function(event) { 			
				$(this).parents('li[role=treeitem]').each(function(event) {
					tree.jstree('select_node', 'tree-item-' + $(this).data("uid"));
				});				
			});
		}
		$('#'+id+' li[aria-selected=true]').each(function(event) { 
						
			uid = $(this).data("uid");
			if(categories!=""){
				categories = categories + "," + uid;
			} else {
				categories = uid;
			}			
		});
		
		hidden.val(categories);		
	});	
}
function mooxNewsFrontendInitFileinput(){
		
	$(".tx-moox-news-frontend input.single-file").each(function(){
		
		maxfilesize = $(this).data("maxfilesize");		
		if(maxfilesize<1){
			maxfilesize = 102400;
		}
		
		accept = $(this).attr("accept");
		if(accept && accept!=''){
			accepts = accept.split(",");
		} else {
			accepts = ['jpg','gif','png','txt','doc','docx','xls','xlsx','ppt','pptx','pdf'];
		}
		
		$(this).fileinput({
			'language':'de',
			'showPreview':true,			
			'showUpload':false,
			'previewFileType':'image',
			'allowedPreviewTypes':['image'],
			'allowedFileExtensions': accepts,
			'maxFileSize': maxfilesize,
			'layoutTemplates': {
				preview: '<div class="file-preview {class}">\n' +													
					'    <div class="file-preview-status text-center text-success"></div>\n' +
					'    <div class="kv-fileinput-error"></div>\n' +					
					'</div>',				
			},
		});
	});
	
	$(".tx-moox-news-frontend input.single-image").each(function(){
		
		maxfilesize = $(this).data("maxfilesize");		
		if(maxfilesize<1){
			maxfilesize = 102400;
		}
		
		accept = $(this).attr("accept");
		if(accept && accept!=''){
			accepts = accept.split(",");
		} else {
			accepts = ['jpg','gif','png'];
		}
		
		$(this).fileinput({
			'language':'de',
			'showPreview':true,			
			'showUpload':false,
			'previewFileType':'image',
			'allowedPreviewTypes':['image'],
			'allowedFileExtensions': accepts,
			'maxFileSize': maxfilesize,
			'layoutTemplates': {
				preview: '<div class="file-preview {class}">\n' +													
					'    <div class="file-preview-status text-center text-success"></div>\n' +
					'    <div class="kv-fileinput-error"></div>\n' +					
					'</div>',				
			},
		});
	});

	$(".tx-moox-news-frontend input.single-file,.tx-moox-news-frontend input.single-image").parent(".btn-file").parent(".input-group-btn").parent(".input-group").find(".file-caption").on("click", function (event) {
		$(this).parent(".input-group").find(".input-group-btn").find(".btn-file").find("input.single-file,input.single-image").click();	
	});	;
	
	$(".tx-moox-news-frontend input#files").on('fileerror', function(event, data) {		
		$('input#files').fileinput('clear');	
	});
	
	$('.tx-moox-news-frontend button.btn-file-add').on('click', function() {
        
		var plugin = $(this).parents('.tx-moox-news-frontend').first().attr('id');
		
		error = false;
		
		form = $(this).parents('form').first();
		name = $(this).attr("name");
		id = $(this).data("id");
		maxitems = $(this).data("maxitems");
		label = $(this).data("label");
		cnt = $("."+id+"_files").length;
		val = $('input[data-id="'+id+'"]').first().val();
		
		mooxNewsFrontendClearErrors(plugin);
		
		if(!error && val==''){
			message = mooxNewsFrontendLang['de'].errors.no_file_selected;
			mooxNewsFrontendAddMessage(label,message,'error','glyphicon-warning-sign',plugin);
			$("html, body").animate({ scrollTop: 0 }, "fast");
			error = true;
		}
		
		if(!error && cnt==maxitems){
			message = mooxNewsFrontendLang['de'].errors.too_many.replace("%1",maxitems);
			mooxNewsFrontendAddMessage(label,message,'error','glyphicon-warning-sign',plugin);
			error = true;			
		} 
		
		if(!error) {
			$('<input>').attr({
				type: 'hidden',
				name: 'client-validation',
				id: 'client-validation',
				value: 'disabled'
			}).appendTo(form);
			$('<input>').attr({
				type: 'hidden',
				name: name,
				value: '1'
			}).appendTo(form);
			form.submit();			
		} else {
			$("html, body").animate({ scrollTop: 0 }, "fast");
		}
    });
	
	$('.tx-moox-news-frontend button.btn-file-remove').on('click', function() {
        
		error = false;
		
		form = $(this).parents('form').first();
		name = $(this).attr("name");		
		
		$('<input>').attr({
			type: 'hidden',
			name: 'client-validation',
			id: 'client-validation',
			value: 'disabled'
		}).appendTo(form);
		$('<input>').attr({
			type: 'hidden',
			name: name,
			value: '1'
		}).appendTo(form);
		form.submit();
		
    });
}
function mooxNewsFrontendInitClientValidation(){
	
	$('.tx-moox-news-frontend form.client-validation').on('submit', function() {
        
		var plugin = $(this).parents('.tx-moox-news-frontend').first().attr('id');
		var validated = true;
		
		if($("#"+plugin+" #client-validation").val()!="disabled"){
			
			$("#"+plugin+" #client-validation").remove();
			
			mooxNewsFrontendClearErrors(plugin);
			
			$('#'+plugin+' form.client-validation').find("input.validate,textarea.validate,select.validate").each(function(){
				
				error = false;
				
				var value = $(this).val();
				name = $(this).attr("name");
				type = $(this).attr("type");
				id = $(this).data("id");
				label = $(this).data("label");
				required = $(this).data("required");
				validator = $(this).data("validator");
				minlength = $(this).data("minlength");
				maxlength = $(this).data("maxlength");
				minitems = $(this).data("minitems");
				maxitems = $(this).data("maxitems");
				maxfilesize = $(this).data("maxfilesize");
				accept = $(this).attr("accept");
				limitlow = $(this).data("limit-low");
				limithigh = $(this).data("limit-high");
				tagname = $(this).prop("tagName").toLowerCase();
				
				if(type=="checkbox"){
					value = '';
					$('#'+plugin+' input[name="'+name+'"]:checked').each(function(){
						if(value!=''){
							value = value+','+$(this).val();
						} else {
							value = $(this).val();
						}
					});					
				}
				
				if((tagname!='select' && value!='' ) || (tagname=='select' && value!='' && value!='0')){
					if(!error && minlength && value.length<minlength){										
						message = mooxNewsFrontendLang['de'].errors.too_short.replace("%1",minlength,plugin);
						mooxNewsFrontendAddMessage(label,message,'error','glyphicon-warning-sign',plugin);				
						error = true;
					}
					if(!error && maxlength && value.length>maxlength){										
						message = mooxNewsFrontendLang['de'].errors.too_long.replace("%1",maxlength,plugin);
						mooxNewsFrontendAddMessage(label,message,'error','glyphicon-warning-sign',plugin);			
						error = true;
					}
					if(!error && limitlow && value<limitlow){										
						message = mooxNewsFrontendLang['de'].errors.too_small.replace("%1",limitlow,plugin);
						mooxNewsFrontendAddMessage(label,message,'error','glyphicon-warning-sign',plugin);			
						error = true;
					}
					if(!error && limithigh && value>limithigh){										
						message = mooxNewsFrontendLang['de'].errors.too_large.replace("%1",limithigh,plugin);
						mooxNewsFrontendAddMessage(label,message,'error','glyphicon-warning-sign',plugin);				
						error = true;
					}
					if(!error && validator=='email' && !mooxNewsFrontendCheckEmail(value)){
						message = mooxNewsFrontendLang['de'].errors.invalid_email;
						mooxNewsFrontendAddMessage(label,message,'error','glyphicon-warning-sign',plugin);					
						error = true;
					}
					if(!error && validator=='password'){
						repetition = name.replace(id,id+"_repeat");
						if($('input[name="'+repetition+'"]').length){
							if(value!=$('input[name="'+repetition+'"]').val()){
								message = mooxNewsFrontendLang['de'].errors.password_not_equal;
								mooxNewsFrontendAddMessage(label,message,'error','glyphicon-warning-sign',plugin);					
								error = true;
							}
						}
					}
					if(!error && validator=='file' && maxitems>0){
						cnt = $("."+id+"_files").length;
						if(cnt==maxitems){
							message = mooxNewsFrontendLang['de'].errors.too_many.replace("%1",maxitems);
							mooxNewsFrontendAddMessage(label,message,'error','glyphicon-warning-sign',plugin);
							error = true;
						}
					}					
					if(!error && validator=='file' && minitems>0){						
						cnt = $("."+id+"_files").length;
						if((cnt+1)<minitems){
							message = mooxNewsFrontendLang['de'].errors.too_few.replace("%1",minitems);
							mooxNewsFrontendAddMessage(label,message,'error','glyphicon-warning-sign',plugin);
							error = true;
						}						
					}					
					if(!error && validator=='tree' && maxitems>0){
						items = value.split(",");
						cnt = items.length;
						if(cnt>maxitems){
							message = mooxNewsFrontendLang['de'].errors.too_many.replace("%1",maxitems);
							mooxNewsFrontendAddMessage(label,message,'error','glyphicon-warning-sign',plugin);
							error = true;
						}
					}					
					if(!error && validator=='tree' && minitems>0){						
						items = value.split(",");
						cnt = items.length;
						if(cnt<minitems){
							message = mooxNewsFrontendLang['de'].errors.too_few.replace("%1",minitems);
							mooxNewsFrontendAddMessage(label,message,'error','glyphicon-warning-sign',plugin);
							error = true;
						}						
					}
				} else {					
					if(validator!='file' && validator!='tree' && required){
						message = mooxNewsFrontendLang['de'].errors.empty;
						mooxNewsFrontendAddMessage(label,message,'error','glyphicon-warning-sign',plugin)					
						error = true;
					} else {
						
						if(validator=='file' && (required || (minitems && minitems>0))){							
							if(!minitems || minitems<1){
								minitems = 1;
							}
							cnt = $("."+id+"_files").length;
							if(cnt<minitems){
								if(minitems==1){
									message = mooxNewsFrontendLang['de'].errors.no_file_selected;
								} else {
									message = mooxNewsFrontendLang['de'].errors.too_few.replace("%1",minitems);
								}
								mooxNewsFrontendAddMessage(label,message,'error','glyphicon-warning-sign',plugin);
								error = true;
							}	
						}						
						if(validator=='tree' && (required || (minitems && minitems>0))){							
							if(!minitems || minitems<1){
								minitems = 1;
							}
							if(value==""){
								cnt = 0;
							} else {
								items = value.split(",");
								cnt = items.length;
							}
							if(cnt<minitems){
								if(minitems==1){
									message = mooxNewsFrontendLang['de'].errors.no_item_selected;
								} else {
									message = mooxNewsFrontendLang['de'].errors.too_few.replace("%1",minitems);
								}
								mooxNewsFrontendAddMessage(label,message,'error','glyphicon-warning-sign',plugin);
								error = true;
							}	
						}
					}
				}
				if(error){
					$(this).parents('.form-group').addClass('has-error');
					validated = false;
				}
				
			});
			
			if(!validated){
				$(".file-add-tmp").remove();
				$("html, body").animate({ scrollTop: 0 }, "fast");		
			}
		}
		
		return validated;
    });
}

function mooxNewsFrontendClearErrors(plugin){
	$('#'+plugin+' form.client-validation').find("input.validate,select.validate").each(function(){
		$(this).parents('.form-group').removeClass('has-error');
	});
	if ( $("#"+plugin+" .typo3-messages").length ) {
		$("#"+plugin+" .typo3-messages").remove();
	}
}
function mooxNewsFrontendAddMessage(title,text,type,icon,plugin){
	if ( !$("#"+plugin+" .typo3-messages").length ) {
		$('<div class="typo3-messages"><div class="typo3-message message-'+type+'"><div class="message-header"><span class="glyphicon '+icon+' icon-alert" aria-hidden="true"></span>'+title+':</div><div class="message-body">'+text+'</div></div></div>').prependTo("#"+plugin);
	} else {
		$('<div class="typo3-message message-'+type+'"><div class="message-header"><span class="glyphicon '+icon+' icon-alert" aria-hidden="true"></span>'+title+':</div><div class="message-body">'+text+'</div></div>').appendTo("#"+plugin+" .typo3-messages");
	}
}
function mooxNewsFrontendCheckEmail(email) {
	var filter = /^([0-9a-zA-Z]([-_\\.]*[0-9a-zA-Z]+)*)@([0-9a-zA-Z]([-_\\.]*[0-9a-zA-Z]+)*)[\\.]([a-zA-Z]{2,9})$/;
	if (!filter.test(email)) {
		return false;
	} else {
		return true;
	}
}