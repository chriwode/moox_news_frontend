$(document).ready(function(){	
	mooxNewsFrontendInitClearButtons();
	mooxNewsFrontendInitComplexify();
	mooxNewsFrontendInitFileinput();
	mooxNewsFrontendInitDatePicker();
	mooxNewsFrontendInitTooltips();
	mooxNewsFrontendInitDropdowns();
	mooxNewsFrontendInitTreeSelector();	
	mooxNewsFrontendInitClientValidation();
	
	$('.tx-moox-news-frontend form select[data-id="type"], .tx-moox-news-frontend form input[data-id="type"]').on('change', function() {
		
		form = $(this).parents('form').first();
		
		name = $(this).attr("name");
		name = name.substr(0, name.indexOf("["));
		name = name+"[reload]";
		
		$('<input>').attr({
			type: 'hidden',
			name: 'client-validation',
			id: 'client-validation',
			value: 'disabled'
		}).appendTo(form);
		
		$('<input>').attr({
			type: 'hidden',
			name: name,
			value: '1'
		}).appendTo(form);
		
		form.submit();
	});
});